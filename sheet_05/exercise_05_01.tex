\section{}





\subsection{}


\begin{proposition}
	\label{invariant iff homomorphism}
	Let~$\glie$ be a Lie~algebra and let~$\beta$ be a bilinear form on~$\glie$.
	The bilinear form~$\beta$ is invariant if and only if the corresponding linear map
	\[
		b
		\colon
		\glie \to \glie^* \,,
		\quad
		x \mapsto \beta(x, \ph)
	\]
	is a homomorphism of representations of~$\glie$.
\end{proposition}


\begin{proof}
	We have the equivalences
	\begin{align*}
		{}&
		\text{ $b$ is a homomorphism of representations }
		\\
		\iff{}&
		\text{ $b(y \act x) = y \act b(x)$ for all~$x, y \in \glie$}
		\\
		\iff{}&
		\text{ $b(y \act x)(z) = (y \act b(x))(z)$ for all~$x, y, z \in \glie$ }
		\\
		\iff{}&
		\text{ $b(y \act x)(z) = - b(x)(y \act z)$ for all~$x, y, z \in \glie$ }
		\\
		\iff{}&
		\text{ $b([y,x], z) = - b(x, [y,z])$ for all~$x, y, z \in \glie$ }
		\\
		\iff{}&
		\text{ $-b([x,y], z) = -b(x, [y,z])$ for all~$x, y, z \in \glie$ }
		\\
		\iff{}&
		\text{ $b([x,y], z) = b(x, [y,z])$ for all~$x, y, z \in \glie$ }
		\\
		\iff{}&
		\text{ $b$ is invariant, }
	\end{align*}
	which proves the assertion.
\end{proof}


The Killing form~$\kappa$ of~$\glie$ is an invariant bilinear form on~$\glie$, and it is non-degenerate because~$\glie$ is simple.
It follows from \cref{invariant iff homomorphism} that~$\kappa$ induces an isomorphism of~\representations{$\glie$}
\[
	k
	\colon
	\glie
	\to
	\glie^* \,,
	\quad
	x
	\mapsto
	\kappa(x, \ph)
\]
Let now~$\beta$ be another invariant bilinear form on~$\glie$.
It follows from \cref{invariant iff homomorphism} that~$\beta$ induces a homomorphism of~\representations{$\glie$}
\[
	b
	\colon
	\glie
	\to
	\glie^* \,,
	\quad
	x
	\mapsto
	\beta(x, \ph) \,.
\]
The composite~$k^{-1} \circ b$ is an endomorphism of the adjoint representation of~$\glie$.
It follows from Schur’s lemma that there exists some scalar~$\lambda$ with~$k^{-1} \circ b = \lambda \id_{\glie}$.
This shows that~$b = \lambda k$, and thus~$\beta = \lambda \kappa$.





\subsection{}


\begin{proposition}
	\label{restrictions of simple lie algebras are again simple}
	Let~$\kf$ be some field and let~$\Kf$ be a field extension of~$\kf$.
	Let~$\glie$ be a simple~\liealgebra{$\Kf$}.
	Then~$\glie$ is also simple as a~\liealgebra{$\kf$}.
\end{proposition}


\begin{proof}
	We know that~$\glie$ is not abelian as a~\liealgebra{$\kf$}, since it is not abelian as a~\liealgebra{$\Kf$}.
	Let~$I$ be a nonzero~\ideal{$\kf$} of~$\glie$.
	We show in the following that~$I = \glie$, which then proves the \lcnamecref{restrictions of simple lie algebras are again simple}

	We observe that the commutator space
	\[
		[\glie, I]_{\kf}
		=
		\gen{
			[x, y]
		\suchthat
			x \in \glie,
			y \in I
		}_{\kf} \,,
	\]
	 which is a priori only a~\linear{$\kf$} subspace of~$\glie$, is already a~\linear{$\Kf$} subspace of~$\glie$ because
	\[
		\lambda [\glie, I]_{\kf}
		=
		[\lambda \glie, I]_{\kf}
		\subseteq
		[\glie, I]_{\kf}
	\]
	for every scalar~$\lambda$ in~$\Kf$.
	But~$[\glie, I]_{\kf}$ is also a~\ideal{$\kf$} of~$\glie$, because both~$\glie$ and~$I$ are~\ideals{$\kf$} of~$\glie$.
	This shows that~$[\glie, I]_{\kf}$ is a~\ideal{$\Kf$} of~$\glie$.

	It follows that~$[\glie, I]_{\kf} = 0$ or~$[\glie, I]_{\kf} = \glie$ because~$\glie$ is simple as a~\liealgebra{$\Kf$}.
	The first case cannot occur because the center of~$\glie$ vanishes, since~$\glie$ is simple as a~\liealgebra{$\Kf$}.
	It follows that~$[\glie, I]_{\kf} = \glie$.
	But~$[\glie, I]_{\kf}$ is contained in~$I$ because~$I$ is a~\ideal{$\kf$} of~$\glie$.
	It follows that~$I = \glie$.
\end{proof}


Let~$\glie$ be any complex, simple Lie~algebra, e.g.~$\sllie(2, \Complex)$.
It follows from \cref{restrictions of simple lie algebras are again simple} that~$\glie$ is also simple as a real Lie~algebra.
The real Killing form~$\kappa$ of~$\glie$ is non-degenerate because~$\glie$ is simple as a real Lie~algebra (and~$\Real$ has characteristic zero).
By using the resulting isomorphism of~\representations{$\glie$} from~$\glie$ to its real dual space~$\glie^*$, we find as in part~a) that
\begin{align*}
	{}&
	\{ \text{invariant bilinear forms~$\glie \times \glie \to \Real$} \}
	\\
	\cong{}&
	\{ \text{homorphisms of real~\representations{$\glie$}~$\glie \to \glie^*$} \}
	\\
	\cong{}&
	\{ \text{homorphisms of real~\representations{$\glie$}~$\glie \to \glie$} \}
	\\
	\cong{}&
	\{ \text{real endomorphisms of the adjoint representation of~$\glie$} \}
\end{align*}
This last term contains all maps of the form~$\lambda \id$ with~$\lambda \in \Complex$, and is therefore at least two-dimensional as a real vector space.
This shown that the real vector space of invariant bilinear forms on~$\glie$ is at least two-dimensional.
This means in particular that not every invariant bilinear form on~$\glie$ is a real scalar multiple of the Killing form.





