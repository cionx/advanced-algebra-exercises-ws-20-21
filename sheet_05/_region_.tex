\message{ !name(sheet_05.tex)}\documentclass[11pt, a4paper, numbers=noenddot]{scrartcl}

\usepackage{../generalstyle}
\usepackage{specificstyle_05}

\titlehead{Group~3, Wednesday~16--18, Tutor: Siu Hang Man \hfill December~4,~2020}
\subject{Solutions for}
\title{Lie~Algebras and Their~Representations}
\subtitle{Sheet~5}
\author{Berthold Lorke \and Jendrik Stelzner}
\date{}

\begin{document}

\message{ !name(exercise_05_04.tex) !offset(-15) }
\section{}





\subsection{}

The representation~$(V, \rho)$ is semisimple by Weyl’s theorem because~$\glie$ is semisimple.
More concretely, if
\[
	\glie = I_1 \oplus \dotsb \oplus I_h
\]
is a decomposition of $\glie$ into simple ideals, then
\[
V = V_1 \oplus \dotsb \oplus V_h
\]
with~$V_i \defined I_i$ for every~$i = 1, \dotsc, h$ is a decomposition of~$V$ into irreducible subrepresentations.
These representations~$V_i$ are pairwise non-isomorphic, as for each index~$j$ distinct from~$i$, the ideal~$I_j$ acts trivially on~$V_i$ but nontrivially on~$V_j$.
(If~$I_j$ were to act trivially on~$V_j$, then~$I_j$ would be abelian.
But the Lie~algebra~$\glie$ is semisimple and therefore contains no nonzero abelian ideal.)
It now follows from Schur’s lemma and~$\kf$ being algebraically closed that
\begin{align*}
	\Kf
	&=
	\Hom_{\glie} (V,V)  \\
	&=
	\Hom_{\glie} ( V_1 \oplus \dotsb \oplus V_h, V_1 \oplus \dotsb \oplus V_h )
	\\
	&\cong
	\begin{pmatrix}
		\Hom_{\glie}(V_1, V_1)  & \cdots  & \Hom_{\glie}(V_h, V_1)  \\
		\vdots                  & \ddots  & \vdots                  \\
		\Hom_{\glie}(V_1, V_h)  & \cdots  & \Hom_{\glie}(V_h, V_h)
	\end{pmatrix}
	\\
	&=
	\begin{pmatrix}
		\kf &         &     \\
		    & \ddots  &     \\
		    &         & \kf
	\end{pmatrix}
	\\
	&\cong
	\kf^h \,.
\end{align*}
as~\algebras{$\kf$}.





\subsection{}

\begin{lemma}
	\label{adjoint property for endomorphisms of adjoint representation}
	Let~$\glie$ be a Lie~algebra and let~$\varphi$ be an endomorphism of the adjoint representation of~$\glie$.
	Then for all~$x, y \in \glie$,
	\[
		[\varphi(x), y]
		=
		\varphi( [x,y] )
		=
		[x, \varphi(y)] \,.
	\]
\end{lemma}

\begin{proof}
	We have
	\[
		\varphi( [x,y] )
		=
		\varphi( x \act y )
		=
		x \act \varphi(y)
		=
		[x, \varphi(y)]
	\]
	and therefore also
	\[
		\varphi( [x,y] )
		=
		- \varphi( [y,x] )
		=
		- [y, \varphi(x)]
		=
		[\varphi(x), y] \,,
	\]
	as claimed.
\end{proof}


\begin{proposition}
	\label{perfect lie algebra has commutative endomorphism algebra}
	Let~$\glie$ be a perfect Lie~algebra.
	Then the~\algebra{$\kf$}~$\End_{\glie}(\glie)$ is commutative.
\end{proposition}

\begin{proof}
	We need to show that any two elements~$\varphi$ and~$\psi$ of~$\End_{\glie}(\glie)$ commute, i.e. that
	\[
		\varphi( \psi(x) )
		=
		\psi( \varphi(x) )
	\]
	for every~$x \in \glie$.
	But~$\glie$ is perfect, whence it sufficies to show that
	\[
		\varphi( \psi( [x,y] ) )
		=
		\psi( \varphi( [x,y] ) )
	\]
	for all~$x, y \in \glie$.
	It follows from \cref{adjoint property for endomorphisms of adjoint representation} that
	\[
		\varphi( \psi( [x,y] ) )
		=
		\varphi( [x, \psi(y)] )
		=
		[\varphi(x), \psi(y)]
		=
		\psi( [\varphi(x), y] )
		=
		\psi( \varphi( [x,y] ) ) \,,
	\]
	as desired.
\end{proof}

\begin{corollary}
	\label{semisimple lie algebra has commutative endomorphism algebra}
	Let~$\glie$ be a semisimple Lie~algebra.
	Then~\algebra{$\kf$}~$\End_{\glie}(\glie)$ is commutative.
	\qed
\end{corollary}

\begin{lemma}
	\label{invariants of extension of scalars}
	Let~$\glie$ be a~\liealgebra{$\kf$} and let~$V$ be representation of$~\glie$, and let~$\Lf$ be a field extension of~$\kf$.
	Then
	\[
		(\Lf \tensor_{\kf} V)^{\Lf \tensor_{\kf} \glie}
		=
		\Lf \tensor V^{\glie} \,.
	\]
\end{lemma}

\begin{proof}
	We may regard~$\glie$ as a~\liesubalgebra{$\kf$} of~$\Lf \tensor \glie$.
	Then
	\[
		(\Lf \tensor_{\kf} V)^{\Lf \tensor_{\kf} \glie}
		=
		(\Lf \tensor_{\kf} V)^{\glie}
	\]
	because~$\glie$ is an~\vectorspace{$\Lf$} generating set of~$\Lf \tensor_{\kf} \glie$.
	For every element~$x$ of~$\glie$ we denote by~$\rho(x)$ the action of~$\glie$ on~$V$, i.e. the map
	\[
		\rho(x)
		\colon
		V
		\to
		V \,,
		\quad
		v
		\mapsto
		x \act v \,.
	\]
	The action of~$x$ on~$L \tensor V$ is then given by~$\id_{\Lf} \tensor \rho(x)$.
	We have
	\[
		\ker( \id_{\Lf} \tensor \rho(x) )
		=
		\ker( \id_{\Lf} ) \tensor_{\kf} V
		+ \Lf \tensor_{\kf} \ker( \rho(x ) )
		=
		\Lf \tensor_{\kf} \ker( \rho(x ) )
	\]
	for every~$x \in \glie$, and thus
	\[
		(\Lf \tensor_{\kf} V)^{\glie}
		=
		\bigcap_{x \in \glie} \ker( \id \tensor \rho(x) )
		=
		\bigcap_{x \in \glie} \Lf \tensor_{\kf} \ker( \rho(x) )
		=
		\Lf \tensor_{\kf} \bigcap_{x \in \glie} \ker( \rho(x) )
		=
		\Lf \tensor_{\kf} V^{\glie} \,,
	\]
	as claimed.
\end{proof}

\begin{proposition}
	\label{extension of scalars of hom representation}
	Let~$\glie$ be a~\liealgebra{$\kf$}, let~$V$ and~$W$ be two representation of$~\glie$ and let~$\Lf$ be a field extension of~$\kf$.
	\begin{enumerate}
		\item
			The~\linear{$\Lf$} map
			\[
				\Phi
				\colon
				\Lf \tensor_{\kf} \Hom_{\kf}(V, W)
				\to
				\Hom_{\Lf}(\Lf \tensor_{\kf} V, \Lf \tensor_{\kf} W) \,,
				\quad
				\lambda \tensor f
				\mapsto
				\lambda \id_{\Lf} \tensor f
			\]
			is a homomorphism of~\representations{$(\Lf \tensor_{\kf} \glie)$}.
		\item
			If at least one of the representations~$V$ and~$W$ is finite-dimensional, then the above map~$\Phi$ resticts to an isomorphism of~\vectorspaces{$\Lf$}
			\[
				\Psi
				\colon
				\Lf \tensor_{\kf} \Hom_{\glie}(V, W)
				\to
				\Hom_{\Lf \tensor \glie}(\Lf \tensor_{\kf} V, \Lf \tensor_{\kf} W) \,.
			\]
	\end{enumerate}
\end{proposition}

\begin{proof}
	\leavevmode
	\begin{enumerate}
		\item
			We regard~$\glie$ as a~\liesubalgebra{$\kf$} of~$\Lf \tensor_{\kf} \glie$.
			It sufficies to show that~$\Phi$ is compatible with the action of~$\glie$ because~$\Lf \tensor_{\kf} \glie$ is spanned by~$\glie$ as an~\vectorspace{$\Lf$}.
			We have
			\begin{align*}
				\Phi(x \act (\lambda \tensor f))(\mu \tensor v)
				&=
				\Phi(\lambda \tensor (x \act f))(\mu \tensor v)
				\\
				&=
				\lambda \mu \tensor ( (x \act f)(v) )
				\\
				&=
				\lambda \mu \tensor (x \act f(v) - f(x \act v))
				\\
				&=
				\lambda \mu \tensor ( x \act f(v) )
				- \lambda \mu \tensor f(x \act v)
				\\
				&=
				x \act ( \lambda \mu \tensor f(v) )
				- \lambda \mu \tensor f(x \act v)
				\\
				&=
				x \act \Phi(\lambda \tensor f)(\mu \tensor v)
				- \Phi(\lambda \tensor f)(\mu \tensor (x \act v))
				\\
				&=
				x \act \Phi(\lambda \tensor f)(\mu \tensor v)
				- \Phi(\lambda \tensor f)(x \act (\mu \tensor v))
				\\
				&=
				(x \act \Phi(\lambda \tensor f))(\mu \tensor v)
			\end{align*}
			for all~$x \in \glie$,~$\lambda, \mu \in \Lf$,~$f \in \Hom_{\kf}(V, W)$,~$v \in V$.
			This proves the assertion.
		\item
			It follows from the additional assumption on the dimensions of~$V$ and~$W$ that the above homomorphism of representations~$\Phi$ is an isomorphism of vector spaces, and therefore an isomorphism of~\representations{$(\Lf \tensor_{\kf} \glie)$}.
			It follows that~$\Phi$ restricts to an isomorphism of vector spaces between the~\invariants{$(\Lf \tensor_{\kf} \glie)$} of both sides.
			It follows on the one hand from \cref{invariants of extension of scalars} that
			\[
				( \Lf \tensor_{\kf} \Hom_{\kf}(V, W) )^{\Lf \tensor_{\kf} \glie}
				=
				\Lf \tensor_{\kf} \Hom_{\kf}(V,W)^{\glie}
				=
				\Lf \tensor_{\kf} \Hom_{\glie}(V, W) \,.
			\]
			But we also have on the other hand that
			\[
				\Hom_{\Lf}(\Lf \tensor_{\kf} V, \Lf \tensor_{\kf} W)^{\Lf \tensor_{\kf} \glie}
				=
				\Hom_{\Lf \tensor_{\kf} \glie}(\Lf \tensor_{\kf} V, \Lf \tensor_{\kf} W) \,.
			\]
			The assertion follows.
		\qedhere
	\end{enumerate}
\end{proof}

\begin{corollary}
	Let~$\glie$ be a finite-dimensional~\liealgebra{$\kf$} and let~$\Lf$ be a field extension of~$\kf$.
	Then
	\[
		\End_{\Lf \tensor_{\kf} \glie}(\Lf \tensor_{\kf} \glie)
		\cong
		\Lf \tensor_{\kf} \End_{\glie}(\glie)
	\]
\end{corollary}

%\begin{proof}
%	We have
%	\begin{align*}
%		\End_{\Lf \tensor_{\kf} \glie}( \Lf \tensor_{\kf} \glie )
%		&=
%		\End_{\Lf}( \Lf \tensor_{\kf} \glie )^{\Lf \tensor \glie}
%		\\
%		&=
%		( \Lf \tensor_{\kf} \End_{\kf}(\glie) )^{\Lf \tensor \glie}
%		\\
%		&=
%		\Lf \tensor_{\kf} \End_{\kf}(\glie)^{\glie}
%		\\
%		&=
%		\Lf \tensor_{\kf} \End_{\glie}(\glie)
%	\end{align*}
%	by \cref{invariants of extension of scalars} and \cref{extension of scalars of hom representation}.
%\end{proof}

\begin{proof}
	The extension of scalars of the adjoint representation of~$\glie$ is precisely the adjoint representation of~$\Lf \tensor_{\kf} \glie$.
	The assertion follows therefore from the second part of \cref{extension of scalars of hom representation} (by choosing both~$V$ and~$W$ as the adjoint representation of~$\glie$).
\end{proof}

If $\glie$ is simple, then the adjoint represenation of~$\glie$ is irreducible.
It then follows from Schur’s lemma that~$\Kf = \End_{\glie}(\glie)$ is a skew field extension of~$\kf$.
As simplicity implies semisimplicity, \cref{semisimple lie algebra has commutative endomorphism algebra} gives us that~$\Kf$ is in fact a field, and therefore a field extension of~$\kf$.

We know from the lectures (without proof) that the extension of scalars~$\kfbar \tensor_{\kf} \glie$ is again semisimple (because~$\kf$ is of characteristic zero and therefore perfect).
It now for the number~$h$ of simple components of~$\kfbar \tensor_{\kf} \glie$ that
\begin{align*}
	h
	&=
	\dim_{\kfbar}( \End_{\kfbar \tensor_{\kf} \glie}( \kfbar \tensor_{\kf} \glie ) )
	\\
	&=
	\dim_{\kfbar}( \kfbar \tensor_{\kf} \End_{\glie}(\glie) )
	\\
	&=
	\dim_{\kf}( \End_{\glie}(\glie) )
	\\
	&=
	\dim_{\kf}(\Kf)
	\\
	&=
	[\Kf : \kf] \,.
\end{align*}





\subsection{}

\begin{proposition}
	\label{lie algebra is simple if its extension of scalars is simple}
	Let~$\glie$ be a~\liealgebra{$\kf$} let~$\Lf$ be a field extension of~$\kf$.
	If the extension of scalars~$\Lf \tensor_{\kf} \glie$ is simple, then~$\glie$ is simple.
\end{proposition}

\begin{proof}
	We may regard~$\glie$ as an~\vectorspace{$\Lf$} generating set of~$\Lf \tensor_{\kf} \glie$.
	The Lie~algebra~$\Lf \tensor_{\kf} \glie$ is nonabelian because it is simple, whence it follows that~$\glie$ is nonabelian.

	Let~$I$ be a nonzero abelian ideal of~$\glie$.
	Then~$\Lf \tensor_{\kf} I$ is a nonzero abelian of~$\glie$.
	It follows that~$\Lf \tensor_{\kf} I = \Lf \tensor_{\kf} \glie$ because~$\Lf \tensor_{\kf} \glie$ is simple, and thus~$I = \glie$ because~$\kf$ is a field.
\end{proof}

\begin{proposition}
	\label{representation is simple if endomorphism algebra is ground field}
	Let~$V$ be a semisimple representation of~$\glie$ with~$\End_{\glie}(V) = \kf$.
	Then~$V$ is irreducible.
\end{proposition}

\begin{proof}
	Let~$V = \bigoplus_{i \in I} V_i$ be a decomposition of~$V$ into irreducible subrepresentations.
	We have for every index~$i$ in~$I$ an endomorphism~$e_i$ of~$V$ given by
	\[
		e_i
		\colon
		V
		\xto{\mathrm{projection}}
		V_i
		\xto{\mathrm{inclusion}}
		V \,.
	\]
	The representation~$V$ is nonzero because~$\End_{\glie}(V) = \kf$ is nonzero.
	There hence exist at least one index~$i$ in~$I$.
	For every index~$j$ in~$I$ both~$e_i$ and~$e_j$ are nonzero elements of~$\End_{\glie}(V) = \kf$, whence their composite~$e_i e_j = \delta_{ij} e_i$ is again nonzero.
	It follows that~$j = i$.
	This shows that~$I = \{ i \}$ and thus~$V = V_i$.
\end{proof}

Suppose first that~$\Kf = \kf$.
It follows from~\cref{representation is simple if endomorphism algebra is ground field} that the adjoint representation of~$\glie$ is irreducible.
This entails that~$\glie$ contains no nonzero ideal.
We also know that~$\glie$ cannot be abelian, since it is semisimple and thus it’s center vanishes.
We thus find that~$\glie$ is simple.
It follows from part~b) (and our proof thereof) that~$\kfbar \tensor_{\kf} \glie$ is semisimple and has precisely one simple component, as~$[\Kf : \kf] = 1$, so~$\kfbar \tensor_{\kf} \glie$ is simple.

Suppose now that~$\kfbar \tensor_{\kf} \glie$ is simple.
Then $\glie$ is simple by \cref{lie algebra is simple if its extension of scalars is simple}.
It follows from part~b) that~$\Kf$ is a field, and that~$[\Kf : \kf] = 1$ because~$\kfbar \tensor_{\kf} \glie$ is simple, so $\Kf = \kf$.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "sheet_05"
%%% End:

\message{ !name(sheet_05.tex) !offset(-399) }

\end{document}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
