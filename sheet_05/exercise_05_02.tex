\section{}





\subsection{}


We show a more general statement.


\begin{proposition}
	Let~$\glie$ and~$\hlie$ be two Lie~algebras.
	Let~$V$ be a representation of~$\glie$ and let~$W$ be a representation of~$\hlie$.
	Then~$V \tensor W$ becomes a representation of~$\glie \oplus \hlie$ via
	\[
		(x,y) \act (v \tensor w)
		\defined
		(x \act v) \tensor w + v \tensor (y \act w)
	\]
	for all~$(x, y) \in \glie \oplus \hlie$ and~$v \in V$,~$w \in W$.
\end{proposition}


\begin{proof}
	The resulting~\module{$\Univ(\glie)$} structure on~$V$ and~\module{$\Univ(\hlie)$} structure on~$W$ combine into a~\module{$(\Univ(\glie) \tensor \Univ(\hlie))$} structure on~$V \tensor W$ given by
	\[
		(x \tensor y) \cdot (v \tensor w)
		=
		(x \cdot v) \tensor (y \tensor w)
	\]
	for all~$x \in \Univ(\glie)$,~$y \in \Univ(\hlie)$ and~$v \in V$,~$w \in W$.
	We recall the isomorphism of algebras
	\begin{align*}
		\Univ(\glie \oplus \hlie)
		&\to
		\Univ(\glie) \tensor \Univ(\hlie)
	\shortintertext{given by}
		(x,y)
		&\mapsto
		x \tensor 1 + 1 \tensor y
	\end{align*}
	for all~$(x, y) \in \glie \oplus \hlie$.
	Under this isomorphism, we get a~\module{$\Univ(\glie \oplus \hlie)$} structure on~$V \tensor W$ given by
	\[
		(x, y) \cdot (v \tensor w)
		=
		(x \cdot v) \tensor w + v \tensor (y \cdot w)
	\]
	for all~$(x,y) \in \glie \oplus \hlie$ and~$v \in V$,~$w \in W$.
	This module structure corresponds to the structure of a~\representation{$(\glie \oplus \hlie)$} on~$V \tensor W$, which is precisely the desired one.
\end{proof}





\subsection{}

The bilinear form~$B_2$ corresponds to a linear map
\[
	b_2
	\colon
	V \tensor V
	\to
	\Complex \,
	\quad
	v \tensor v'
	\mapsto
	B_2(v, v') \,.
\]
This linear map induces a linear map
\[
	b
	\colon
	(V \tensor V) \tensor (V \tensor V)
	\xto{\mathrm{swap}}
	V \tensor V \tensor V \tensor V
	\xto{b_2 \tensor b_2}
	\Complex \tensor \Complex
	\xto{\mathrm{mult}}
	\Complex
\]
given by
\[
	(v \tensor w) \tensor (v' \tensor w')
	\mapsto
	v \tensor v' \tensor w \tensor w'
	\mapsto
	B_2(v, v') \tensor B_2(w, w')
	\mapsto
	B_2(v, v') B(w, w') \,.
\]
This linear map~$b$ corresponds to a bilinear form on~$V \tensor V$, which is precisely the desired bilinear form~$B$.

For every two vectors~$v$ and~$v'$ in~$V$ we denote by~$(v,v')$ the matrix of size~$2 \times 2$ whose first column is~$v$ and whose second column is~$v'$.
The bilinear form~$B_2$ is given by
\begin{equation}
	\label{bilinear form via determinant}
	B_2(v,v')
	=
	\det{} (v,v') \,.
\end{equation}
We see from this description that~$B_2$ is skew-symmetric.
It follows that the bilinear form~$B$ is symmetric:
it sufficies to check this on a vector space generating set of~$V \tensor V$, and we have
\begin{align*}
	B( v \tensor w, v' \tensor w' )
	&=
	B_2(v, v') B(w, w')
	\\
	&=
	(- B_2(v', v) ) (- B(w', w) )
	\\
	&=
	B_2(v', v) B_2(w', w)
	\\
	&=
	B( v' \tensor w', v \tensor w )
\end{align*}
for all~$v, v', w, w' \in V$.

With respect to the ordered basis~$(e_1 \tensor e_1, e_1 \tensor e_2, e_2 \tensor e_1, e_2 \tensor e_2)$ of~$V \tensor V$ the bilinear form~$B$ is given by the matrix
\[
	\begin{pmatrix*}[r]
		0 &  0  &  0  & 1 \\
		0 &  0  & -1  & 0 \\
		0 & -1  &  0  & 0 \\
		1 &  0  &  0  & 0
	\end{pmatrix*}
\]
This matrix is invertible, whence~$B$ is non-degenerate.
This matrix is also symmetric, which proves again that~$B$ is symmetric.

It remains to show that the bilinear form~$B$ is~\invariant{$(\sllie(2, \Complex) \oplus \sllie(2, \Complex))$}.
Instead of doing explicit calculations that hold over every field, we (ab)use that we are working over the complex numbers.

Let~$(x,y)$ be an element of~$\sllie(2, \Complex) \oplus \sllie(2, \Complex)$ and let~$\gamma_x$ and~$\gamma_y$ be two smooth curves in~$\SL(2, \Complex)$ with~$\gamma(0) = \Id$ and~$\gamma_x'(0) = x$, and similarly~$\gamma_y(0) = \Id$ and~$\gamma_y'(0) = y$.
We may for example choose
\[
	\gamma_x
	\colon
	\Real
	\to
	\SL(2, \Complex) \,,
	\quad
	t
	\mapsto
	\exp(tx) \,,
\]
and similarly for the curve~$\gamma_y$.%
\footnote{
	We use here that~$\det(\exp(A)) = \exp(\tr(A))$ for every complex matrix~$A$.
	It follows from this identity that the proposed formula does indeed define a curve in~$\SL(2, \Complex)$.
}
The vector space~$V$ is the natural (smooth) representation of the group~$\SL(2, \Complex)$.
The tensor product~$V \tensor V$ becomes therefore a (smooth) representation of the group~$\SL(2, \Complex) \times \SL(2, \Complex)$ via the formula
\[
	(g, h) \act (v \tensor w)
	\defined
	(gv) \tensor(hw)
\]
for all~$(g,h) \in \SL(2, \Complex) \times \SL(2, \Complex)$ and~$v \in V$,~$w \in W$.
The two smooth curves~$\gamma_x$ and~$\gamma_y$ in~$\SL(2, \Complex)$ correspond to a smooth curve~$\gamma$ in~$\SL(2, \Complex) \times \SL(2, \Complex)$ given by
\[
	\gamma(t)
	\defined
	( \gamma_x(t), \gamma_y(t) )
\]
for every~$t \in \Real$.
We now find that
\begin{align*}
	\der \gamma(t) \act (v \tensor w)
	&=
	\der (\gamma_x(t), \gamma_y(t)) \act (v \tensor w)
	\\
	&=
	\der (\gamma_x(t) v) \tensor (\gamma_y(t) w)
	\\
	&=
	(\gamma'_x(0) v) \tensor (\gamma_y(0) w)
	+ (\gamma_x(0) v) \tensor (\gamma'_y(0) w)
	\\
	&=
	(x v) \tensor (\Id w) + (\Id v) \tensor (y w)
	\\
	&=
	(x v) \tensor w + v \tensor (y w)
	\\
	&=
	(x,y) \act (v \tensor w)
\end{align*}
for all~$v \in V$ and~$w \in W$, and thus
\[
	\der \gamma(t) \act u
	=
	(x,y) \act u
\]
for every~$u \in V \tensor W$ by linearity.
These calculations show that the action of~$\sllie(2, \Complex) \oplus \sllie(2, \Complex)$ on~$V \tensor W$ comes from the (smooth) action of~$\SL(2, \Complex) \times \SL(2, \Complex)$ on~$V \tensor V$.

We see from the description~\eqref{bilinear form via determinant} of the original bilinear form~$B_2$ on~$V$ that this bilinear form is~\invariant{$\SL(2, \Complex)$}.
It follows from this observation that the resulting bilinear form~$B$ on~$V \tensor V$ is~\invariant{$\SL(2, \Complex) \times \SL(2, \Complex)$}.
This entails that for any two element~$u$ and~$u'$ of~$V \tensor W$ the smooth map
\[
	\Real
	\to
	\Complex \,,
	\quad
	t
	\mapsto
	B
	\Bigl(
		\gamma(t) \act u,
		\gamma(t) \act u',
	\Bigr)
\]
is constant.
It follows that
\begin{align*}
	0
	&=
	\der
	B
	\Bigl(
		\gamma(t) \act u,
		\,
		\gamma(t) \act u'
	\Bigr)
	\\[0.3em]
	&=
	B
	\Bigl(
		\der
		\gamma(t) \act u,
		\,
		\gamma(0) \act u'
	\Bigr)
	+
	B
	\Bigl(
		\gamma(0) \act u,
		\,
		\der
		\gamma(t) \act u'
	\Bigr)
	\\[0.3em]
	&=
	B( (x,y) \act x, \, \Id \act u' )
	+ B( \Id \act u, \, (x,y) \act u' )
	\\
	&=
	B( (x,y) \act u, u')
	+ B( u, (x,y) \act u' ) \,.
\end{align*}
This shows that the bilinear map~$B$ is~\invariant{$(\sllie(2, \Complex) \oplus \sllie(2, \Complex))$}.





\subsection{}

The bilinear form~$B$ on~$V \tensor V$ is symmetric and non-degenerate, and the field~$\Complex$ is quadratically closed with characteristic distinct from~$2$.
It follows that there exists an orthonormal basis~$u_1, u_2, u_3, u_4$ of~$V \tensor V$ with respect to~$B$.

Let~$\rho$ be the homomorphism of Lie~algebras from~$\sllie(2, \Complex) \oplus \sllie(2, \Complex)$ to~$\gllie(V \tensor V)$ that corresponds to the action of~$\sllie(2, \Complex) \oplus \sllie(2, \Complex)$ on~$V \tensor V$.
We have seen before that the bilinear form~$B$ is~\invariant{$(\sllie(2, \Complex) \oplus \sllie(2, \Complex))$}.
This tells us that with respect to this orthonormal basis of~$V \tensor V$, the image of~$\rho$ is containted in~$\orthlie(4, \Complex)$.





\subsection{}

In the previous parts of this exercise, we have already constructed a homomorphism of Lie~algebras from~$\sllie(2, \Complex) \oplus \sllie(2, \Complex)$ to~$\orthlie(4, \Complex)$.
Both Lie~algebras are six-dimensional.
It therefore sufficies to show that the representation~$(V \tensor V, \rho)$ is faithful.
The above homomorphism of Lie~algebras from~$\sllie(2, \Complex) \oplus \sllie(2, \Complex)$ to~$\orthlie(4, \Complex)$ is then injective, and thus an isomorphism by dimensions.

We have for~$\sllie(2, \Complex) \oplus \sllie(2, \Complex)$ the basis
\[
	(e, 0) \,,
	\quad
	(h, 0) \,,
	\quad
	(f, 0) \,,
	\quad
	(0, e) \,,
	\quad
	(0, h) \,,
	\quad
	(0, f) \,,
\]
and for~$V \tensor V$ the basis
\[
	e_1 \tensor e_1 \,,
	\quad
	e_1 \tensor e_2 \,
	\quad
	e_2 \tensor e_1 \,
	\quad
	e_2 \tensor e_2 \,.
\]
With respect these bases we have
\begin{alignat*}{3}
	\rho( (e,0) )
	&\equiv
	\begin{pmatrix}
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
		0 & 0 & 0 & 0 \\
		0 & 0 & 0 & 0
	\end{pmatrix} \,,
	&\quad
	\rho( (h,0) )
	&\equiv
	\begin{pmatrix*}[r]
		1 & 0 &  0  &  0  \\
		0 & 1 &  0  &  0  \\
		0 & 0 & -1  &  0  \\
		0 & 0 &  0  & -1
	\end{pmatrix*}
	&\quad
	\rho( (f,0) )
	&\equiv
	\begin{pmatrix}
		0 & 0 & 0 & 0 \\
		0 & 0 & 0 & 0 \\
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0
	\end{pmatrix}
	\\
	\rho( (0,e) )
	&\equiv
	\begin{pmatrix}
		0 & 1 & 0 & 0 \\
		0 & 0 & 0 & 0 \\
		0 & 0 & 0 & 1 \\
		0 & 0 & 0 & 0
	\end{pmatrix} \,,
	&\quad
	\rho( (0,h) )
	&\equiv
	\begin{pmatrix*}[r]
		1 &  0  & 0 &  0  \\
		0 & -1  & 0 &  0  \\
		0 &  0  & 1 &  0  \\
		0 &  0  & 0 & -1
	\end{pmatrix*} \,,
	&\quad
	\rho( (0,f) )
	&\equiv
	\begin{pmatrix}
		0 & 0 & 0 & 0 \\
		1 & 0 & 0 & 0 \\
		0 & 0 & 0 & 0 \\
		0 & 0 & 1 & 0
	\end{pmatrix} \,,
\end{alignat*}
These matrices are linearly independent, which shows that the representation~$(V \tensor V, \rho)$ is faithful.





