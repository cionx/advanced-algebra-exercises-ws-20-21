%%%%% PACKAGES

\usepackage{microtype}

\usepackage{xparse}

\usepackage[dvipsnames]{xcolor}

\usepackage{mathtools}
\usepackage{amsthm}

\usepackage[%
	warnings-off = {{mathtools-colon, mathtools-overbracket}},
	partial = upright
]{unicode-math}

\usepackage{tikz-cd}

\usepackage{csquotes}

\usepackage{enumitem}

\usepackage{hyperref}

\usepackage[capitalize, noabbrev]{cleveref}





%%%%% CONFIGURATION

% don’t number subsubsections

\setcounter{secnumdepth}{2}

% fonts
\setmainfont[
	Numbers = OldStyle
]{Libertinus Serif}
\setsansfont[%
	Numbers = OldStyle
]{Libertinus Sans}
\setmathfont{Libertinus Math}
\setmonofont[Scale=MatchLowercase]{Inconsolata}

% adjust tikzcd arrows to font
\usepackage{../arrowstyle}

% spacing
\frenchspacing
\binoppenalty = \maxdimen
\relpenalty   = \maxdimen

% show overfull hboxes
\overfullrule = 2mm

% names of (sub)sections
\renewcommand{\thesection}{Exercise~\arabic{section}}
\renewcommand{\thesubsection}{\alph{subsection})}

% theorem-like environments
\newcounter{everything}
\theoremstyle{definition}
\newtheorem{corollary}[everything]{Corollary}
\newtheorem{definition}[everything]{Definition}
\newtheorem{lemma}[everything]{Lemma}
\newtheorem{proposition}[everything]{Proposition}
\newtheorem{recall}[everything]{Recall}
\newtheorem{remark}[everything]{Remark}

% color of links
\hypersetup{
		colorlinks,
		linkcolor = {MidnightBlue},
		% alternative for citecolor: RedViolet
		citecolor = {BrickRed},
		urlcolor  = {BrickRed}
}



%%%%% LISTS

\setlist[enumerate]{
	wide          = 0pt,
	leftmargin    = *,
	listparindent = \parindent,
	parsep        = 0pt
}
\setlist[enumerate, 1]{
	label = {\arabic*.},
	ref   = {\arabic*}
}
\setlist[enumerate, 2]{
	label = {\alph*.},
	ref   = {\alph*}
}

\setlist[itemize]{
	wide          = 0pt,
	leftmargin    = *,
	listparindent = \parindent,
	parsep        = 0pt
}
\setlist[itemize, 1]{
	label = {\textbullet}
}
\setlist[itemize, 2]{
	label = {\textopenbullet}
}

\newlist{itemize*}{itemize}{2}
\setlist[itemize*]{
	wide          = 0pt,
	leftmargin    = *,
	labelindent   = \parindent,
	listparindent = \parindent,
	parsep        = 0pt
}
\setlist[itemize*, 1]{
	label = {\textbullet}
}
\setlist[itemize*, 2]{
	label = {\textopenbullet}
}



%%%%% WORDS

\newcommand{\adnilpotent}{$\ad$\nobreakdash-nilpotent}
\newcommand{\adsemisimple}{$\ad$\nobreakdash-semisimple}
\newcommand{\algebra}[1]{#1\nobreakdash-algebra}
\newcommand{\algebras}[1]{#1\nobreakdash-algebras}
\newcommand{\bilinear}[1]{#1\nobreakdash-bilinear}
\newcommand{\bimodules}[2]{#1\nobreakdash-#2\nobreakdash-\hspace{0pt}bimodules}
\newcommand{\eigenspace}[1]{#1\nobreakdash-eigenspace}
\newcommand{\grading}[1]{#1\nobreakdash-grading}
\newcommand{\ideal}[1]{#1\nobreakdash-ideal}
\newcommand{\ideals}[1]{#1\nobreakdash-ideals}
\newcommand{\invariant}[1]{#1\nobreakdash-invariant}
\newcommand{\invariants}[1]{#1\nobreakdash-invariants}
\newcommand{\liealgebra}[1]{#1\nobreakdash-Lie~algebra}
\newcommand{\liesubalgebra}[1]{#1\nobreakdash-Lie~subalgebra}
\newcommand{\linear}[1]{#1\nobreakdash-linear}
\newcommand{\many}[1]{#1\nobreakdash-many}
\newcommand{\matrices}[1]{#1\nobreakdash-matrices}
\newcommand{\module}[1]{#1\nobreakdash-module}
\newcommand{\nth}[1]{#1\nobreakdash-th}
\newcommand{\representation}[1]{#1\nobreakdash-\hspace{0pt}representation}
\newcommand{\representations}[1]{#1\nobreakdash-\hspace{0pt}representations}
\newcommand{\submodule}[1]{#1\nobreakdash-\hspace{0pt}submodule}
\newcommand{\subrepresentation}[1]{#1\nobreakdash-\hspace{0pt}subrepresentation}
\newcommand{\subrepresentations}[1]{#1\nobreakdash-\hspace{0pt}subrepresentations}
\newcommand{\valued}[1]{#1\nobreakdash-valued}
\newcommand{\vectorspace}[1]{#1\nobreakdash-vector space}
\newcommand{\vectorspaces}[1]{#1\nobreakdash-vector spaces}
\newcommand{\weightspace}[1]{#1\nobreakdash-weight space}



%%%%% TEXT FORMATTING

\newcommand{\defemph}{\emph}



%%%%% COMMANDS

% arrows
\renewcommand{\to}{
  \mathchoice
  {\longrightarrow}
  {\rightarrow}
  {\rightarrow}
  {\rightarrow}
}
\AtBeginDocument{
  \let\oldmapsto\mapsto
  \renewcommand{\mapsto}{
    \mathchoice
    {\longmapsto}
    {\oldmapsto}
    {\oldmapsto}
    {\oldmapsto}
  }
}
\newcommand{\xto}[1]{
	\mathchoice
	{\xrightarrow{\enspace #1 \enspace}}
	{\xrightarrow{#1}}
	{\xrightarrow{#1}}
	{\xrightarrow{#1}}
}

% numbers
\newcommand{\numberfont}{\mathbb}
\newcommand{\Natural}{\numberfont{N}}
\newcommand{\Integer}{\numberfont{Z}}
\newcommand{\Rational}{\numberfont{Q}}
\newcommand{\Real}{\numberfont{R}}
\newcommand{\Complex}{\numberfont{C}}

% fields
\newcommand{\kf}{\mathbb{k}}
\newcommand{\kfbar}{\overline{\kf}}
\newcommand{\Kf}{\mathbb{K}}
\newcommand{\Lf}{\mathbb{L}}
\newcommand{\Finite}{\mathbb{F}}

% lie algebras
\newcommand{\liefont}{\mathfrak}
% generic names
\newcommand{\alie}{\liefont{a}}
\newcommand{\blie}{\liefont{b}}
\newcommand{\glie}{\liefont{g}}
\newcommand{\hlie}{\liefont{h}}
\newcommand{\klie}{\liefont{k}}
\newcommand{\qlie}{\liefont{q}}
\newcommand{\slie}{\liefont{s}}
% special names
\newcommand{\gllie}{\liefont{gl}}
\newcommand{\orthlie}{\liefont{o}}
\newcommand{\sorthlie}{\liefont{so}}
\newcommand{\sllie}{\liefont{sl}}
\newcommand{\trianglie}{\liefont{t}}
\newcommand{\upperlie}{\liefont{n}}

% spaces
\DeclareMathOperator{\centerlie}{Z}
\DeclareMathOperator{\Der}{Der}
\DeclareMathOperator{\End}{End}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\Irr}{L}
\DeclareMathOperator{\Mat}{M}
\DeclareMathOperator{\Prim}{P}
\DeclareMathOperator{\SL}{SL}
\DeclareMathOperator{\spann}{span}
\newcommand{\symm}{\mathrm{S}}
\DeclareMathOperator{\Symm}{S}
\DeclareMathOperator{\Triang}{T}
\DeclareMathOperator{\Univ}{U}

% relations
\newcommand{\defined}{\coloneqq}

% binary operations
\newcommand{\act}{\mathbin{.}}
\newcommand{\dsum}{\oplus}
\newcommand{\quot}{\mathbin{/}}
\newcommand{\tensor}{\otimes}

% special elements
\newcommand{\Id}{\mathbb{1}}

% functions
\DeclareMathOperator{\ad}{ad}
\DeclareMathOperator{\ch}{ch}
\newcommand{\id}{\mathrm{id}}
\NewDocumentCommand{\pder}{ O{} m }{
  \mathchoice
  {\frac{\partial #1}{\partial #2}}
  {\partial #1 / \partial #2}
  {\partial #1 / \partial #2}
  {\partial #1 / \partial #2}
}
\NewDocumentCommand{\ppder}{ O{} m }{
  \mathchoice
  {\frac{\partial^2 #1}{\partial #2^2}}
  {\partial #1 / \partial #2}
  {\partial #1 / \partial #2}
  {\partial #1 / \partial #2}
}
\DeclareMathOperator{\tr}{tr}

% delimiters
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\gen}{\langle}{\rangle}
\DeclarePairedDelimiter{\genideal}{\lparen}{\rparen}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}

% sets
\providecommand\given{}
\newcommand\SetSymbol[1][]{%
  \nonscript\:#1\vert
  \allowbreak
  \nonscript
  \:\mathopen{}}
\DeclarePairedDelimiterX\set[1]\{\}{%
  \renewcommand\given{\SetSymbol[\delimsize]}
  #1
}

% differential stuff
\newcommand{\dd}[1]{\mathopen{}\,\mathrm{d}#1}

% others
\newcommand{\ph}{-}

% decoration
\newcommand{\comp}{\mathrm{c}}
\newcommand{\trans}{{\mathsf{t}}}

% suchthat
\NewDocumentCommand%
{\suchthat}%
{so}
{%
  \IfValueTF{#2}
  {%
    \:#2|\:
  }
  {%
    \IfBooleanTF{#1}
    {%
      \:\middle|\:
    }
    {%
      \mathrel{|}      
    }
  }
}



