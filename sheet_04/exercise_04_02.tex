\section{}





\subsection{}

That~$D$ is a split endomorphism means (by definition) that there exists a nonzero polynomial~$f$ in~$\kf[t]$ with~$f(D) = 0$ such that~$f$ decomposes into linear factors.
It follows that~$A$ decomposes into the generalized eigenspaces with respect to~$D$, from which it further follows that~$D$ admits a Jordan--Chevalley decomposition~$D = D_s + D_n$ such that~$D_s$ is diagonalizable,~$D_n$ is nilpotent, and~$D_s$ and~$D_n$ commute with each other.
For every scalar~$\lambda$, the~\eigenspace{$\lambda$} of~$D_s$ is given by the generalized~\eigenspace{$\lambda$} of~$D$.
We denote this linear subspace of~$A$ by~$A_\lambda$.

It sufficies to show that the diagonalizable part~$D_s$ is again a derivation.
It then follows that the difference~$D - D_s$ is also a derivation, i.e. that the nilpotent part~$D_n$ is again a derivation.

To show that~$D_s$ is a derivation of~$A$ we need to show that
\begin{equation}
	\label{derivation condition}
	D_s(xy)
	=
	D_s(x) y + x D_s(y)
	\qquad
	\text{for all~$x, y \in A$.}
\end{equation}
It follows from the decomposition~$A = \bigoplus_{\lambda \in \kf} A_\lambda$ that it sufficies to show the condition~\eqref{derivation condition} for~$x$ in~$A_\lambda$ and~$y$ in~$A_\mu$, where~$\lambda$ and~$\mu$ are two scalars.
We then have
\[
	D_s(x) y + x D_s(y)
	=
	\lambda xy + \mu xy
	=
	(\lambda + \mu) xy \,.
\]
We hence need to show that
\[
	D_s(xy)
	=
	(\lambda + \mu) xy
	\qquad
	\text{for all~$x \in A_\lambda$ and~$y \in A_\mu$.}
\]
In other words, we need to show that
\[
	A_\lambda A_\mu
	\subseteq
	A_{\lambda + \mu} \,.
\]
This will follows from the following~\lcnamecref{product of generalized eigenspaces of a derivation}.

\begin{proposition}
	\label{product of generalized eigenspaces of a derivation}
	Let~$A$ be a~\algebra{$\kf$}, let~$D$ be a derivation of~$A$, and for every scalar~$\lambda$ in~$\kf$ let~$A_\lambda$ be the generalized~\eigenspace{$\lambda$} of~$D$.
	Then~$A_\lambda A_\mu \subseteq A_{\lambda + \mu}$ for all~$\lambda, \mu \in \kf$.
\end{proposition}

To prove this \lcnamecref{product of generalized eigenspaces of a derivation} we use some basic observations.

\begin{lemma}
	\label{functoriality of generalized eigenspace}
	Let~$V$ and~$V'$ be two vector spaces.
	Let~$f$ be an endomorphism of~$V$ and let~$f'$ be an endomorphism of~$V'$.
	Let~$g$ be a linear map from~$V$ to~$V'$ with~$f' \circ g = g \circ f$, i.e. such that the following square diagram commutes.
	\[
		\begin{tikzcd}
			V
			\arrow{r}[above]{f}
			\arrow{d}[left]{g}
			&
			V
			\arrow{d}[right]{g}
			\\
			V'
			\arrow{r}[above]{f'}
			&
			V'
		\end{tikzcd}
	\]
	For every scalar~$\lambda$ let~$V_\lambda$ be the generalized~\eigenspace{$\lambda$} of~$f$, and let similarly~$V'_\lambda$ be the generalized~\eigenspace{$\lambda$} of~$f'$.
	The linear map~$g$ maps for every scalar~$\lambda$ the generalized eigenspace~$V_\lambda$ into the generalized eigenspace~$V'_\lambda$.
\end{lemma}


\begin{proof}
	Let~$v$ be an element of~$V_\lambda$.
	There exists some exponent~$k$ with~$(f - \lambda \id)^k(v) = 0$.
	It follows that
	\[
		(f' - \lambda \id)^k( g(v) )
		=
		g( (f - \lambda \id)^k(v) )
		=
		g( 0 )
		=
		0 \,,
	\]
	which shows that~$g(v)$ is contained in~$V'_\lambda$.
\end{proof}


\begin{lemma}
	\label{generalized eigenspace of tensor sum}
	Let~$V$ and~$W$ be two vector spaces, let~$f$ be an endomorphism of~$V$ and let~$g$ be an endomorphism of~$W$.
	For every scalar~$\lambda$ let~$V_\lambda(f)$ be the generalized~\eigenspace{$\lambda$} of~$f$, and similarly for~$W_\lambda(g)$.
	Then~$V_\lambda(f) \tensor W_\mu(g)$ is contained in the generalized~\eigenspace{$(\lambda + \mu)$} of~$f \tensor \id + \id \tensor g$, i.e.
	\[
		V_\lambda(f) \tensor W_\mu(g)
		\subseteq
		(V \tensor W)_{\lambda + \mu}(f \tensor \id + \id \tensor g) \,.
	\]
\end{lemma}


\begin{proof}
	We have
	\begin{align*}
		(f \tensor \id + \id \tensor g) - (\lambda + \mu) \id_{V \tensor W}
		&=
		(f \tensor \id + \id \tensor g) - (\lambda + \mu) \id \tensor \id
		\\
		&=
		f \tensor \id + \id \tensor g - (\lambda \id) \tensor \id - \id \tensor (\mu \id)
		\\
		&=
		(f - \lambda \id) \tensor \id + \id \tensor (g - \mu \id)
	\end{align*}
	and therefore
	\begin{align*}
		\Bigl( (f \tensor \id + \id \tensor g) - (\lambda + \mu) \id \Bigr)^n
		&=
		\Bigl( (f - \lambda \id) \tensor \id + \id \tensor (g - \mu \id) \Bigr)^n
		\\
		&=
		\sum_{m=0}^n
		\binom{n}{m}
		\Bigl( (f - \lambda \id) \tensor \id \Bigr)^m
		\Bigl( \id \tensor (g - \mu \id) \Bigr)^{n-m}
		\\
		&=
		\sum_{m=0}^n
		\binom{n}{m}
		( f - \lambda \id )^m \tensor  ( g - \mu \id )^{n-m}
	\end{align*}
	for every exponent~$n$ since the two endomorphisms~$(f - \lambda \id) \tensor \id$ and~$\id \tensor (g - \mu \id)$ of~$V \tensor W$ commute with each other.

	Let now~$v$ be an element of~$V_\lambda(f)$ and let~$w$ be an element of~$W_\mu(g)$.
	There exist exponents~$k$ and~$l$ with
	\[
		(f - \lambda)^k(v) = 0
		\quad\text{and}\quad
		(g - \mu)^l(w) = 0 \,.
	\]
	It follows for every exponent~$n$ with~$n \geq k+l-1$ that
	\[
		\Bigl( (f \tensor \id + \id \tensor g) - (\lambda + \mu) \id \Bigr)^n (v \tensor w)
		=
		\sum_{m=0}^n
		\binom{n}{m}
		(f - \lambda \id)^m(v) \tensor (g - \mu \id)^{n-m}(w)
		=
		0
	\]
	because every summand vanishes, since for every~$m = 0, \dotsc, n$ we have~$m \geq k$ or~$n-m \geq l$.
	This shows that the simple tensor~$v \tensor w$ is contained in the generalized~\eigenspace{$(\lambda + \mu)$} of~$f \tensor \id + \id \tensor g$.
\end{proof}


\begin{proof}[Proof of \cref{product of generalized eigenspaces of a derivation}]
	Let
	\[
		m
		\colon
		A \tensor A
		\to
		A \,,
		\quad
		x \tensor y
		\mapsto
		xy
	\]
	be the multiplication map of~$A$.
	That~$D$ is a derivation of~$A$ means that
	\[
		D \circ m
		=
		m \circ (D \tensor \id + \id \tensor D) \,,
	\]
	i.e. that the following square diagram commutes.
	\[
		\begin{tikzcd}[column sep = 5.5em]
			A \tensor A
			\arrow{r}[above]{D \tensor \id + \id \tensor D}
			\arrow{d}[left]{m}
			&
			A \tensor A
			\arrow{d}[right]{m}
			\\
			A
			\arrow{r}[above]{D}
			&
			A
		\end{tikzcd}
	\]
	It follows from \cref{functoriality of generalized eigenspace} and \cref{generalized eigenspace of tensor sum} that
	\[
		A_\lambda A_\mu
		=
		m( A_\lambda \tensor A_\mu )
		\subseteq
		m( (A \tensor A)_{\lambda + \mu} )
		\subseteq
		A_{\lambda + \mu} \,,
	\]
	where~$(A \tensor A)_{\lambda + \mu}$ denotes the generalized~\eigenspace{$(\lambda + \mu)$} of~$D \tensor \id + \id \tensor D$.
\end{proof}





\subsection{}

We have as before that~$x_s$ is diagonalizable and that~$x_n$ is nilpotent.
We have
\[
	\ad(x)
	=
	\ad(x_s + x_n)
	=
	\ad(x_s) + \ad(x_n) \,,
\]
and the endomorphisms~$\ad(x_s)$ and~$\ad(x_n)$ commute with each other because
\[
	[ \ad(x_s), \ad(x_n) ]
	=
	\ad( [x_s, x_n] )
	=
	\ad( 0 )
	=
	0 \,.
\]
It therefore remains to show that~$\ad(x_s)$ is diagonalizable and that~$\ad(x_n)$ is nilpotent.

Let~$s$ be a diagonalizable endomorphism of~$V$.
Let~$v_1, \dotsc, v_n$ be a basis of~$V$ consisting of eigenvectors of~$s$ and let~$\lambda_i$ be the eigenvalue for~$v_i$.
The algebra~$\End(V)$ has the endomorphisms~$E_{ij}$ with~$i, j = 1, \dotsc, n$ as a basis, where~$E_{ij}(e_k) = \delta_{jk} e_i$ for every~$k = 1, \dotsc, n$.
For these basis endomorphisms we have
\[
	\ad(s)(E_{ij})
	=
	[s, E_{ij}]
	=
	(\lambda_i - \lambda_j) E_{ij}
\]
for all~$i, j = 1, \dotsc, n$.%
\footnote{
	This can be directly computed by expressing~$s$ as a diagonal matrix and~$E_{ij}$ as the standard basis matrix of the same name.
}
This shows that~$\End(V)$ admits a basis given by eigenvectors of~$\ad(s)$.
The endomorphism~$\ad(s)$ of the vector space~$\End(V)$ is therefore again diagonalizable.

Let~$n$ be a nilpotent endomorphism of~$V$.
The endomorphisms~$\lambda$ and~$\rho$ of~$\End(V)$ given by
\[
	\lambda(f) \defined n \circ f
	\quad\text{and}\quad
	\rho(f) \defined f \circ n
\]
are again nilpotent and commute each other.
It follows that~$\ad(n) = \lambda - \rho$ is again nilpotent.





%%% Local Variables:
%%% mode: latex
%%% TeX-master: "sheet_04"
%%% End:
