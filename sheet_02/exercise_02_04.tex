\section{}

By a~\enquote{\algebra{$\kf$}} we mean a~\vectorspace{$\kf$}~$A$ together with a~\bilinear{$\kf$} multiplication map
\[
	A \times A
	\to
	A \,,
	\quad
	(a,b)
	\mapsto
	a \cdot b \,.
\]
By a~\algebra{$\kf$} we mean an associative, unitial~\enquote{\algebra{$\kf$}}.


\subsection{}

\begin{proposition}
	\label{derivation determined by algebra generators}
	Let~$A$ be a~\enquote{\algebra{$\kf$}}, and let~$\delta$ be a derivation of~$A$.
	The kernel of~$\delta$ is a subalgebra of~$A$.
	If~$A$ is unital then this entails that~$\ker(\delta)$ is a unital subalgebra of~$A$.
\end{proposition}

\begin{proof}
	The kernel of~$\delta$ is a linear subspace of~$A$ because~$\delta$ is linear.
	For any two elements~$a$ and~$b$ in~$\ker(\delta)$ we have
	\[
		\delta(ab)
		=
		\delta(a) b + a \delta(b)
		=
		0 \cdot b + a \cdot 0
		=
		0 \,,
	\]
	which shows that~$\ker(\delta)$ is closed under multiplication.
	If~$A$ is unital then it follows from the identity~$1 = 1 \cdot 1$ that
	\[
		\delta(1)
		=
		\delta(1 \cdot 1)
		=
		\delta(1) \cdot 1 + 1 \cdot \delta(1)
		=
		2 \delta(1) \,.
	\]
	This shows that~$\delta(1) = 0$, so that~$1$ is contained in~$\ker(\delta)$.
\end{proof}

\begin{corollary}
	Let~$A$ be an \enquote{algebra} and let~$\delta_1$ and~$\delta_2$ be two derivations of~$A$ that coincide on a set of algebra generators of~$A$.
	Then~$\delta_1$ and~$\delta_2$ are already identical.
\end{corollary}

\begin{proof}
	The difference~$\delta_1 - \delta_2$ is again a derivation of~$A$, whence its kernel is a (unital) subalgebra of~$A$.
	This subalgebra contains a set of algebra generators of~$A$ by assumption, and hence equals all of~$A$.
	We have shown that~$\ker(\delta_1 - \delta_2) = A$, and thus~$\delta_1 = \delta_2$.
\end{proof}

\begin{lemma}
	\label{dervation acting on inverse}
	Let~$A$ be a~\algebra{$\kf$}, let~$\delta$ be a derivation of~$A$ and let~$a$ be a unit in~$A$.
	Then~$\delta(a^{-1})$ is uniquely determined by~$\delta(a)$.
\end{lemma}

\begin{proof}
	It follows from the identity~$1 = a \cdot a^{-1}$ that
	\[
		0
		=
		\delta(1)
		=
		\delta(a \cdot a^{-1})
		=
		\delta(a) a^{-1} + a \delta(a^{-1}) \,,
	\]
	and thus~$\delta(a^{-1}) = - a^{-1} \delta(a) a^{-1}$.
\end{proof}

\begin{proposition}
	\label{derivations form a submodule}
	Let~$A$ be a commutative, associative~\algebra{$\kf$}.
	Then~$\Der(A)$ is an~\submodule{$A$} of~$\End_{\kf}(A)$.
\end{proposition}

\begin{proof}
	We already know that~$\Der(A)$ is a linear subspace of~$\End_{\kf}(A)$.
	For every element~$a$ of~$A$ and every derivation~$\delta$ of~$A$ we have
	\begin{align*}
		(a \delta)(b c)
		&=
		a \delta(b c)
		\\
		&=
		a ( \delta(b) c + b \delta(c) )
		\\
		&=
		a \delta(b) c + a b \delta(c)
		\\
		&=
		a \delta(b) c + b a \delta(c)
		\\
		&=
		(a \delta)(b) c + b (a \delta)(c)
	\end{align*}
	This shows that~$a \delta$ is again a derivation of~$A$, which in turn shows that~$\Der(A)$ is a closed under the action of~$A$ on~$\End_{\kf}(A)$.
\end{proof}

Let from now on~$A$ be the~\algebra{$\Complex$} of Laurent polynomials~$\Complex[Z, Z^{-1}]$.
It follows from the product rule for~$\pder{z}$ that this is a derivation of~$A$.
It follows from \cref{derivations form a submodule} that there exists for every element~$f$ of~$A$ a derivation~$\delta$ of~$A$ with~$\delta(Z) = f$, namely the derivation~$f \cdot \pder{Z}$.

The algebra~$A$ is generated (as a unital algebra) by the two elements~$Z$ and~$Z^{-1}$.
It follows from \cref{derivation determined by algebra generators} and \cref{dervation acting on inverse} that every derivation~$\delta$ of~$A$ is uniqely determined by its action on the element~$Z$ of~$A$.
For the element~$f \defined \delta(Z)$ we thus have~$\delta = f \cdot \pder{Z}$.

This shows altogether that~$\Der(A)$ is free as an~\module{$A$} with basis~$\pder{Z}$.

\begin{remark}
	One can show in the same way for~$B = \kf[X_1, \dotsc, X_n]$ or~$B = \kf[X_1, X_1^{-1}, \dotsc, X_n, X_n^{-1}]$ that~$\Der(B)$ is free as a~\module{$B$} with basis given by~$\pder{X_1}, \dotsc, \pder{X_n}$.
\end{remark}

%\subsection{}
%
%Let $D$ be a derivation.
%
%\begin{lemma}
%	We have $D(Z^n) = D(Z)nZ^{n-1}$ for $n\in \Natural_0$.
%\end{lemma}
%\begin{proof}
%	We do this by induction.
%	We see via $D(Z) = D(Z \cdot 1) = D(Z) + D(1)Z$ that $D(1)=0$, which is the claim for $n=0$.
%	Now assume that the claim $D(Z^n) = D(Z)nZ^{n-1}$ holds for some $n \in \Natural_0$.
%	We then calculate
%	\begin{align*}
%		D(Z^{n+1})
%		&=
%		D(Z \cdot Z^n)
%		\\
%		&=
%		D(Z)Z^n + D(Z^n)Z
%		\\
%		&=
%		D(Z)Z^n + D(Z)\cdot n D(Z)Z^{n-1}Z
%		\\
%		&=
%		D(Z)(n+1)Z^n \,.
%	\end{align*}
%	This concludes the induction.
%\end{proof}
%
%\begin{lemma}
%	We have $D(Z^{-n}) = D(Z) \cdot (-n)Z^{-n-1}$ for $n \in \Natural_{>0}$.
%\end{lemma}
%\begin{proof}
%	We show this by induction.
%	With $D(1) = D(ZZ^{-1}) = 0$ from the previous lemma, we have
%	\begin{align*}
%		D(Z^{-1}) &=
%		D(Z^{-1}) - Z^{-1}D(ZZ^{-1}) \\ &=
%		D(Z^{-1}) - Z^{-1}(D(Z)Z^{-1} + D(Z^{-1})Z) \\ &=
%		D(Z) \cdot (-1)Z^{-2} \,,
%	\end{align*}
%	which is the claim for $n=1$.
%	Assume now that the claim $D(Z^{-n}) = D(Z) \cdot (-n)Z^{-n-1}$ holds for some $n \in \Natural_0$.
%	We then calculate
%	\begin{align*}
%		D(Z^{-(n+1)}) &=
%		D(Z^{-1}Z^{-n}) \\ &=
%		D(Z^{-1})Z^{-n} + D(Z^{-n})Z^{-1} \\ &=
%		D(Z)\cdot(-1)Z^{-2}Z^{-n} + D(Z)\cdot (-n)Z^{-n-1}Z^{-1} \\ &=
%		D(Z) \cdot (n+1)Z^{-(n+1)-1} \,.
%	\end{align*}
%	This concludes the induction.
%\end{proof}
%
%The two lemmas together give us $D(Z^n) = D(Z)\cdot n D(Z^{-1})$ for all $n \in \Integer$, so by $k$-linearity, we have $D = f \cdot \pder{Z}$ for some $f \in V$, and we indeed have $f = D(Z)$.

Given the derivation~$\delta \defined \pder{Z}$ and any two elements~$f$ and~$g$ of~$A$ we find that
\[
	(f \delta)( (g \delta)(h) )
	=
	f \cdot \delta(g \cdot \delta(h) )
	=
	f \cdot \bigl( \delta(g) \cdot \delta(h) + g \cdot \delta^2(h) \bigr)
	=
	f \delta(g) \delta(h) + f g \delta^2(h)
\]
for every~$h \in A$.
It follows that
\[
	[f \delta, g \delta](h)
	=
	f \delta(g) \delta(h) + f g \delta^2(h)
	-
	g \delta(f) \delta(h) - g f \delta^2(h)
	=
	\bigl( f \delta(g) - \delta(f) g\bigr) \delta(h)
\]
for every~$h \in A$, and thus
\begin{equation}
	\label{commutator of two derivations}
	\biggl[ f \pder{Z}, g \pder{Z} \biggr]
	=
	\biggl( f \pder[g]{Z} - \pder[f]{Z} g \biggr) \pder{Z} \,.
\end{equation}



\subsection{}

\begin{proposition}
	\label{commutator of derivation and multiplication}
	Let~$A$ be a~\algebra{$\kf$}, let~$f$ be an element of~$A$ and let~$\delta$ be a derivation of~$A$.
	Then
	\[
		[\delta, f \Id]
		=
		\delta(f) \Id \,.
	\]
\end{proposition}

\begin{proof}
	We have for every element~$g$ of~$A$ that
	\[
		[\delta, f \Id](g)
		=
		\delta(f g) - f \delta(g)
		=
		\delta(f) g + f \delta(g) - f \delta(g)
		=
		\delta(f) g \,,
	\]
	and thus~$[\delta, f \Id] = \delta(f) \Id$ as claimed.
\end{proof}

It follows from the identity \eqref{commutator of two derivations} and \cref{commutator of derivation and multiplication} that
\begin{align*}
	[E, F]
	&=
	\biggl[ \pder{Z}, - Z^2 \pder{Z} + \lambda Z \Id \biggr]
	\\
	&=
	- \biggl[ 1 \pder{Z}, Z^2 \pder{Z} \biggr] + \lambda \biggl[ \pder{Z}, Z \Id \biggr]
	\\
	&=
	- \biggl( 1 \pder[Z^2]{Z} - \pder[1]{Z} Z^2 \biggr) \pder{Z} + \lambda \pder[Z]{Z} \Id
	\\
	&=
	- 2 Z \pder{Z} + \lambda \Id
	\\
	&=
	H \,,
\end{align*}
as well as
\begin{align*}
	\SwapAboveDisplaySkip
	[H, E]
	&=
	\biggl[ -2 Z \pder{Z} + \lambda \Id, \pder{Z} \biggr]
	\\
	&=
	- 2 \biggl[ Z \pder{Z}, 1 \pder{Z} \biggr] + \lambda \biggl[ \Id, \pder{Z} \biggr]
	\\
	&=
	- 2 \biggl( Z \pder[1]{Z} - \pder[Z]{Z} 1 \biggr) \pder{Z} + 0
	\\
	&=
	- 2 (Z \cdot 0 - 1 \cdot 1) \pder{Z}
	\\
	&=
	2 \pder{Z}
	\\
	&=
	2 E \,,
\end{align*}
and finally
\begin{align*}
	\SwapAboveDisplaySkip
	[H, F]
	&=
	\biggl[ -2 Z \pder{Z} + \lambda \Id, - Z^2 \pder{Z} + \lambda Z \Id \biggr]
	\\
	&=
	2 \biggl[ Z \pder{Z}, Z^2 \pder{Z} \biggr]
	- 2 \lambda \biggl[ Z \pder{Z}, Z \Id \biggr]
	- \lambda  \biggl[ \Id, Z^2 \pder{Z} + \lambda Z \biggr]
	\\
	&=
	2 \biggl( Z \pder[Z^2]{Z} - \pder[Z]{Z} Z^2 \biggr) \pder{Z}
	- 2 \lambda Z \pder[Z]{Z} \Id
	- 0
	\\
	&=
	2 ( 2 Z^2 - Z^2 ) \pder{Z} - 2 \lambda Z \Id
	\\
	&=
	2 Z^2 \pder{Z} - 2 \lambda Z \Id
	\\
	&=
	- 2 F \,.
\end{align*}
This shows that the elements~$E$,~$H$,~$F$ of~$\gllie(V)$ satisfy the same commutator relations as the standard basis elements~$e$,~$h$,~$f$ of~$\sllie(2, \Complex)$.
The unique linear map~$\rho_\lambda$ from~$\sllie(2, \Complex)$ to~$\gllie(V)$ given by
\[
	e
	\mapsto
	E \,,
	\quad
	h
	\mapsto
	H \,,
	\quad
	f
	\mapsto
	F
\]
is therefore a homomorphism of Lie~algebras.
This homomorphism makes the vector space~$V$ into a representation of~$\sllie(2, \Complex)$.



\subsection{}

We will in the following show that the representation~$(V, \rho)$ of~$\sllie(2, \Complex)$ is never irreducible, and will classify all of its submodules.
We write~$V$ instead of~$(V, \rho)$.

\subsubsection*{General discussion}

For every scalar~$\mu$ in~$\Complex$ let
\[
	V_\mu
	\defined
	\{
	  v \in V
	\suchthat
		h \act v = \mu v
	\}
\]
be the weight space of~$V$ for the weight~$\mu$.
We observe that
\[
	h \act Z^n
	=
	\biggl( - 2 Z \pder{Z} + \lambda \Id \biggr) Z^n
	=
	- 2 Z \pder[Z^n]{Z} + \lambda \Id(Z^n)
	=
	- 2 n Z Z^{n-1} + \lambda Z^n
	=
	(\lambda - 2 n) Z^n \,.
\]
It follows that the representation~$V$ admits the weight space decomposition
\[
	V
	=
	\bigoplus_{n \in \Integer}
	V_{\lambda + 2n}
\]
with each weigth space being one-dimensional.
We know from the lectures that the action of~$e$ on~$V$ maps the weight space~$V_\mu$ into the weight space~$V_{\mu + 2}$, and that the action of~$f$ on~$V$ maps the weight space~$V_{\mu}$ into the weight space~$V_{\mu - 2}$.
The representation~$V$ does therefore look as in \cref{image of representation}, and on basis elements ups to scalars as in \cref{image of representation on basis vectors}.

\begin{figure}
	\[
		\begin{tikzcd}
			\dotsb
			\arrow[bend left, dashed]{r}
			&
			V_{\lambda - 4}
			\arrow[bend left, dashed]{r}
			\arrow[loop above, distance=3em]{}
			\arrow[bend left, dotted]{l}
			&
			V_{\lambda - 2}
			\arrow[bend left, dashed]{r}
			\arrow[loop above, distance=3em]{}
			\arrow[bend left, dotted]{l}
			&
			V_{\lambda}
			\arrow[bend left, dashed]{r}
			\arrow[loop above, distance=3em]{}
			\arrow[bend left, dotted]{l}
			&
			V_{\lambda + 2}
			\arrow[bend left, dashed]{r}
			\arrow[loop above, distance=3em]{}
			\arrow[bend left, dotted]{l}
			&
			V_{\lambda + 4}
			\arrow[bend left, dashed]{r}
			\arrow[loop above, distance=3em]{}
			\arrow[bend left, dotted]{l}
			&
			\dotsb
			\arrow[bend left, dotted]{l}
		\end{tikzcd}
	\]
	\caption{%
		The representation~$V$.
		The action of~$e$ is depicted by dashed arrows, the action of~$f$ by dotted arrows, and the action of~$h$ by solid arrows.}
	\label{image of representation}
\end{figure}

\begin{figure}
	\[
		\begin{tikzcd}
			\dotsb
			\arrow[bend left, dashed]{r}
			&
			X^2
			\arrow[bend left, dashed]{r}
			\arrow[loop above, distance=3em]{}
			\arrow[bend left, dotted]{l}
			&
			X^1
			\arrow[bend left, dashed]{r}
			\arrow[loop above, distance=3em]{}
			\arrow[bend left, dotted]{l}
			&
			X^0
			\arrow[bend left, dashed]{r}
			\arrow[loop above, distance=3em]{}
			\arrow[bend left, dotted]{l}
			&
			X^{-1}
			\arrow[bend left, dashed]{r}
			\arrow[loop above, distance=3em]{}
			\arrow[bend left, dotted]{l}
			&
			X^{-2}
			\arrow[bend left, dashed]{r}
			\arrow[loop above, distance=3em]{}
			\arrow[bend left, dotted]{l}
			&
			\dotsb
			\arrow[bend left, dotted]{l}
		\end{tikzcd}
	\]
	\caption{%
		The representation~$V$ via basis vectors.
		The action of~$e$ is depicted by dashed arrows, the action of~$f$ by dotted arrows, and the action of~$h$ by solid arrows.
		These actions hold only up to scalars.
	}
	\label{image of representation on basis vectors}
\end{figure}

Suppose now that~$U$ is a subrepresentation of~$V$.
The action of~$h$ on~$V$ is diagonalizable, whence its restriction to~$U$ is again diagonalizable.%
\footnote{
	Here we use one of the authors’ favourite facts from linear algebra:
	the restriction of a diagonalizable endomorphism to an invariant linear subspace is again diagonalizable.
}
This means that~$U$ inherits from~$V$ a weight space decomposition, with the weight spaces of~$U$ being given by~$U_\mu = U \cap V_\mu$ for every scalar~$\mu$ in~$\Complex$.
The weight spaces~$V_\mu$ are either zero or one-dimensional, so it follows for every scalar~$\mu$ that the weight space~$U_\mu$ is either zero or equal to~$V_\mu$.
From the above explicit computation of the weight spaces~$V_\mu$ we hence find the following:

\begin{quote}
	\centering
	\parbox{0.7\textwidth}{
		Every subrepresentation of~$V$ is a direct sum of weight~spaces of~$V$,
		and has thus a basis given by some~$Z^m$s.
	}
\end{quote}

To understand the subrepresentations of~$V$ we hence need to understand which direct sums of weight spaces of~$V$ are closed under the actions of~$e$,~$h$ and~$f$.
For this we need to understand what collections of~$Z^m$s are closed under the actions of~$e$,~$h$ and~$f$ up to scalar.

We don’t have to worry about the action of~$h$ since each~$Z^m$ is an eigenvector for this action, and is thus mapped to a scalar multiple of itself.
But the actions of~$e$ and~$f$ shift weight spaces.
We hence need to understand when this shift is the zero map.
For this we need to understand the kernels of the action of~$e$ and~$f$ on~$V$, i.e. the kernels of the endomorphisms~$E$ and~$F$.

\begin{proposition}
	\label{kernels of e and f}
	\leavevmode
	\begin{enumerate}
		\item
			The kernel of~$E$ is one-dimensional and spanned by~$1 = X^0$.
		\item
			The kernel of~$F$ is zero if~$\lambda$ is not an integer, and it is one-dimensional and spanned by~$X^m$ if~$\lambda = m$.
	\end{enumerate}
\end{proposition}

\begin{proof}
	\leavevmode
	\begin{enumerate}
		\item
			This holds because the field~$\Complex$ is of characteristic zero.
		\item
			The multiplication with~$-Z$ is a vector space monomorphism of~$V$.
			It follows that
			\[
				\ker(F)
				=
				\ker\biggl( - Z^2 \pder{Z} + \lambda Z \Id \biggr)
				=
				\ker\biggl( (-Z) \biggl( \lambda \Id - Z \pder{Z} \biggr) \biggr)
				=
				\ker\biggl( \lambda \Id - Z \pder{Z} \biggr) \,.
			\]
			This space is precisely the eigenspace of the endomorphism~$Z \cdot \pder{Z}$ of~$V$ for the potential eigenvalue~$\lambda$.
			We observe that the endomorphism~$Z \cdot \pder{Z}$ of~$V$ is diagonalizable:
			a basis of eigenvectors is given by the Laurent polynomials~$Z^m$ with~$m \in \Integer$, with corresponding eigenvalue~$m$.
			We hence find that~$\ker(F)$ is zero if~$\lambda$ is not an integer, and spanned by~$Z^m$ if~$\lambda = m$ is an integer.
		\qedhere
	\end{enumerate}
\end{proof}

We can now classify the subrepresentations of~$V$.
(The following argumentation is somewhat messy and not very elegant, as we had some trouble keeping track of all the possible cases.
It would probably be a good idea to draw pictures for visual intuation, but we currently don’t have the time for that at the)

\subsubsection*{If $\lambda$ is not an integer}

Suppose first that~$\lambda$ is not an integer.
We see from \cref{kernels of e and f} and \cref{image of representation on basis vectors} that if a subrepresentation of~$V$ contains some~$Z^m$, the it also contains~$Z^n$ for all~$n \geq m$ by the action of~$f$.
It also follows from \cref{kernels of e and f} and \cref{image of representation on basis vectors} that if a subrepresentation~$U$ of~$V$ contains some~$Z^m$ with~$m < 0$, then also contains~$Z^n$ for all~$n \leq m$ by the action of~$e$;
if it contains~$Z^m$ for some~$m \geq 0$, then it also contains~$Z^n$ for all~$n = 0, \dotsc, m$ by the action of~$e$.

We get from this the following for any subrepresentation~$U$ of~$V$:
\begin{itemize*}
	\item
		If~$U$ contains~$Z^m$ for some~$m \geq 0$, then by the action of~$f$ it contains~$Z^n$ for all~$n \geq m$, and thus by the action of~$e$ all~$Z^n$ with~$n \geq 0$.
	\item
		If~$U$ contains~$Z^m$ with~$m < 0$, then by the action of~$f$ it contains all~$Z^n$ with~$m \geq n$, and by the action of~$e$ it also contains all~$Z^n$ with~$n \leq m$.
		It thus contains~$Z^n$ for all~$n \in \Integer$.
\end{itemize*}
We therefore get precisely the following subrepresentations of~$V$ if~$\lambda$ is not an integer.
\[
	0 \,,
	\qquad
	\gen{X^n \suchthat n \geq 0}
	=
	\Complex[Z] \,,
	\qquad
	\gen{Z^n \suchthat n \in \Integer}
	=
	\Complex[Z, Z^{-1}] \,.
\]

\subsubsection*{If $\lambda$ is an integer}

Suppose now that~$\lambda$ is an integer, and let us write~$k$ instead of~$\lambda$.

We see from \cref{kernels of e and f} and \cref{image of representation on basis vectors} that if a subrepresentation of~$V$ contains~$Z^m$ for some~$m \leq k$, then it it also contains~$Z^n$ for all~$n = m, \dotsc, k$ by the action of~$f$;
if it contains~$Z^m$ for some~$m > k$, then it also contains~$Z^n$ for all~$n \geq m$.
As in the previous case, it also follows from \cref{kernels of e and f} and \cref{image of representation on basis vectors} that if a subrepresentation~$U$ of~$V$ contains~$Z^m$ with~$m < 0$, then also contains~$Z^n$ for all~$n \leq m$ by the action of~$e$;
if it contains~$Z^m$ for some~$m \geq 0$, then it also contains~$Z^n$ for all~$n = 0, \dotsc, m$ by the action of~$e$.

This means that both~$f$ and~$e$ have one problematic position:
we can’t get from~$X^0$ to~$X^{-1}$ with~$e$, and we can’t get from~$X^k$ to~$X^{k+1}$ with~$f$.
To see how this affects the subrepresentations of~$V$ we need to see how these two problematic positions lie relative to each other.
For this, we distinguish between the following two subsubcases.

\paragraph{If~$k$ is negative,}
then we get for any subrepresentation~$U$ of~$V$ the following:
\begin{itemize}
	\item
		If~$U$ contains~$Z^m$ for some~$m \leq k$ then it contains~$Z^k$ by the action of~$f$, and thus~$Z^n$ for all~$n \leq k$ by the action of~$e$.
	\item
		If~$U$ contains~$Z^m$ for some~$k < m < 0$, then it contains~$Z^n$ for all~$n \geq m$ by the action of~$f$, and~$Z^n$ for all~$n \leq m$ by the action of~$e$.
		It thus contains~$Z^n$ for all~$n \in \Integer$.
	\item
		If~$U$ contains~$Z^m$ for some~$m \geq 0$ then it contains~$Z^0$ by the action of~$e$, and thus~$Z^n$ for all~$n \geq 0$ by the action of~$f$.
\end{itemize}
We get in this subsubcase the following subrepresentations of~$V$:
\begin{gather*}
	0 \,,
	\qquad
	\gen{ Z^n \suchthat n \leq k } \,,
	\qquad
	\gen{ Z^n \suchthat n \geq 0 }
	=
	\Complex[Z]
	\,,
	\\[0.5em]
	\gen{ Z^n \suchthat \text{$n \leq k$ or~$n \geq 0$} }
	=
	\gen{ Z^n \suchthat n \leq k } \oplus \Complex[Z] \,,
	\qquad
	\gen{ Z^n \suchthat n \in \Integer }
	=
	\Complex[Z, Z^{-1}] \,.
\end{gather*}

\paragraph{If~$k$ is non-negative,}
then we get for any subrepresentation~$U$ of~$V$ the following:
\begin{itemize}
	\item
		If~$U$ contains~$Z^m$ for some~$m < 0$ then it contains~$Z^{-1}$ by the action of~$f$ and thus~$Z^n$ for all~$n < 0$ by the action of~$e$.
		It also contains~$Z^n$ for~$n = 0, \dotsc, k$ by the action of~$f$, and thus altogether~$Z^n$ for all~$n \leq k$.
	\item
		If~$U$ contains~$Z^m$ for some~$0 \leq m \leq k$ then it contains~$Z^k$ by the action of~$f$, and thus~$Z^n$ for all~$n = 0, \dotsc, k$ by the action of~$e$.
	\item
		If~$U$ contains~$Z^m$ for some~$m > k$ then it contains~$Z^n$ for all~$n \geq m$ by the action of~$f$, and thus~$Z^n$ for all~$n \geq 0$ by the action of~$e$.
\end{itemize}
We get in this subcase the following subrepresentations of~$V$:
\begin{gather*}
	\gen{ Z^n \suchthat n \leq k } \,,
	\qquad
	\gen{ Z^n \suchthat n = 0, \dotsc, k } \,,
	\qquad
	\gen{ Z^n \suchthat n \geq 0 }
	=
	\Complex[Z] \,,
	\\[0.5em]
	\gen{ Z^n \suchthat n \in \Integer }
	=
	\Complex[Z, Z^{-1}] \,.
\end{gather*}





%%% Local Variables:
%%% mode: latex
%%% TeX-master: "sheet_02"
%%% End:
