\section{}

The commutator relations
\[
	[h,e] = 2e \,,
	\quad
	[h,f] = -2f \,,
	\quad
	[e,f] = h
\]
become in~$\Univ(\sllie(2, \kf))$ the identities
\[
	he - eh = 2e \,,
	\quad
	hf - fh = -2f \,,
	\quad
	ef - fe = h \,.
\]
We can regard these identities as rewritting rules
\[
	eh \to he - 2e \,,
	\qquad
	hf \to fh - 2f \,,
	\qquad
	ef \to fe + h \,.
\]





\subsection*{The term $ehf$}

It follows that
\begin{align*}
	\SwapAboveDisplaySkip
	ehf
	&=
	(he - 2e)f
	\\
	&=
	hef - 2ef
	\\
	&=
	h(fe + h) - 2(fe + h)
	\\
	&=
	hfe + h^2 - 2fe - 2h
	\\
	&=
	(fh - 2f)e + h^2 - 2fe - 2h
	\\
	&=
	fhe - 2fe + h^2 - 2fe - 2h
	\\
	&=
	fhe - 4fe + h^2 - 2h \,.
\end{align*}





\subsection*{The term~$h f^3$}

Let us now show that
\begin{equation}
	\label{general formula}
	h f^n
	=
	f^n h - 2 n f^n
\end{equation}
for every natural number~$n$.
This can be done by straightforward induction:
The equality holds for~$n = 0$, and if the equality holds for some natural number~$n$ then it follows from the calculation
\begin{align*}
	\SwapAboveDisplaySkip
	h f^{n+1}
	&=
	h f^n f
	\\
	&=
	(f^n h - 2n f^n) f
	\\
	&=
	f^n h f - 2n f^{n+1}
	\\
	&=
	f^n (fh - 2f) - 2n f^{n+1}
	\\
	&=
	f^{n+1} h - 2 f^{n+1} - 2n f^{n+1}
	\\
	&=
	f^{n+1} h - 2 (n+1) f^{n+1} \,.
\end{align*}
that the equality also holds for~$n+1$.

\begin{remark}
	One can also prove the formula~\eqref{general formula} without induction:
	The map
	\[
		[h, -]
		\colon
		\Univ(\sllie(2, \kf))
		\to
		\Univ(\sllie(2, \kf))
	\]
	is an algebra derivation, in the sense that
	\[
		[a, xy]
		=
		[a,x] y + x [a,y]
	\]
	for every two elements~$x$ and~$y$ of~$\Univ(\sllie(2, \kf))$.
	It follows that
	\[
		[h, f^n]
		=
		[h, \underbrace{f \dotsm f}_n]
		=
		\sum_{i=0}^{n-1}
		\underbrace{f \dotsm f}_i [h, f] \underbrace{f \dotsm f}_{n-i-1}
		=
		\sum_{i=0}^{n-1}
		\underbrace{f \dotsm f}_i (-2 f) \underbrace{f \dotsm f}_{n-i-1}
		=
		- 2 n f^n \,.
	\]
	By rearraging the equation
	\[
		h f^n - f^n h
		=
		[h, f^n]
		=
		- 2 n f^n
	\]
	we arrive at the claimed equation~$h f^n = f^n h - 2 n f^n$.
\end{remark}

It follows from the general formula~\eqref{general formula} that
\[
	h f^3 = f^3 h - 6 f^3 \,.
\]





\subsection*{The term~$h^3 f$}

We have
\begin{align*}
	hf
	&=
	fh - 2f \,,
\shortintertext{and therefore}
	h^2 f
	&=
	h (fh - 2f)
	\\
	&=
	hfh - 2hf
	\\
	&=
	(fh - 2f)h - 2(fh - 2f)
	\\
	&=
	fh^2 - 2fh - 2fh + 2f
	\\
	&=
	fh^2 - 4fh + 4f \,,
\shortintertext{and thus}
	h^3 f
	&=
	h (fh^2 - 4fh + 4f)
	\\
	&=
	hfh^2 - 4hfh + 4hf
	\\
	&=
	(fh - 2f)h^2 - 4(fh - 2f)h + 4(fh - 2f)
	\\
	&=
	fh^3 - 2fh^2 - 4fh^2 + 8fh + 4fh - 8f
	\\
	&=
	fh^3 - 6fh^2 + 12fh - 8f \,.
\end{align*}





\subsection*{The term~$e^2 h^2 f^2$}

For this last term we use the power of modern technology.



\subsubsection*{sage}

We can use sage.
\begin{sagecommandline}
sage: g = lie_algebras.three_dimensional_by_rank(QQ, 3, names=['E','F','H'])

sage: def sort_key(x):
....:   if x == 'F':
....:     return 0
....:   if x == 'H':
....:     return 1
....:   if x == 'E':
....:     return 2

sage: pbw = g.pbw_basis(basis_key=sort_key)
sage: E,F,H = pbw.algebra_generators()

sage: E^2 * H^2 * F^2
\end{sagecommandline}
It is now easy to see that
\begin{align*}
	e^2 h^2 f^2
	&=
	f^2 h^2 e^2
	- 16 f^2 h e^2
	+ 64 f^2 e^2
	\\
	&\phantom{=}
	+ 4 f h^3 e
	- 56 f h^2 e
	+ 240 f h e
	- 288 f e
	\\
	&\phantom{=}
	+ 2 h^4
	- 18 h^3
	+ 48 h^2
	- 32 h \,.
\end{align*}



\subsubsection*{python}

We can also use python’s sympy library, which allows for symbolic manipulation.
\begin{pythoncode}
from sympy import *

e, h, f = symbols('e h f', commutative=False)

def sl2expand(term):
  while True:
    newterm = term
    newterm = newterm.subs(e*h, h*e - 2*e)
    newterm = newterm.subs(h*f, f*h - 2*f)
    newterm = newterm.subs(e*f, f*e + h)
    newterm = expand(newterm)
    if newterm == term:
      break
    else:
      term = newterm
  return term
\end{pythoncode}
This program gives us the following result.
\begin{consoleoutput}
>>> sl2expand(e**2 * h**2 * f**2)
-288*f*e + 240*f*h*e - 56*f*h**2*e + 4*f*h**3*e + 64*f**2*e**2 - 16*f**2*h*e**2 + f**2*h**2*e**2 - 32*h + 48*h**2 - 18*h**3 + 2*h**4
\end{consoleoutput}
This is the same result that we got from sage.





