\section{}





\subsection{}

We know from the lectures that the tensor power~$V^{\tensor n}$ inherits from~$V$ the structure of a~\representation{$\glie$}, given by
\[
	x \act (v_1 \tensor \dotsb \tensor v_n)
	=
	\sum_{i=1}^n
	v_1 \tensor \dotsb \tensor v_{i-1} \tensor (x \act v_i) \tensor v_{i+1} \tensor \dotsb \tensor v_n
\]
for all~$x \in \glie$ and~$v_1, \dotsc, v_n \in V$.
We show in the following that the linear subspace~$I$ of~$V^{\tensor n}$ (as defined on the exercise sheet) is a subrepresentation.
This then shows that the symmetric power~$\Symm^n(V)$ can be made into a quotient representation of the tensor power~$V^{\tensor n}$.

We need to show that for every element~$u$ of~$I$ and every element~$x$ of~$\glie$ the element~$x \act u$ is again contained in~$I$.
By the definition of~$I$ (and the linearity of the action of~$x$ on~$V^{\tensor n}$) we may assume that the element~$u$ is of the form
\[
	u
	=
	v_1 \tensor \dotsb \tensor v_n
	- v_{\sigma(1)} \tensor \dotsb \tensor v_{\sigma(n)} 
\]
for some vectors~$v_1, \dotsc, v_n$ in~$V$.
We consider now the right action of the symmetric group~$\symm_n$ on the tensor power~$V^{\tensor n}$ that is given by
\[
	(w_1 \tensor \dotsb \tensor w_n) \act \sigma
	=
	w_{\sigma(1)} \tensor \dotsb \tensor w_{\sigma(n)}
\]
for all~$w_1, \dotsc, w_n \in V$ and~$\sigma \in \symm_n$.
Moreover, we denote for all~$i = 1, \dotsc, n$ by~$T_i$ the endomorphism of~$V^{\tensor n}$ that is given by
\[
	T_i(w_1 \tensor \dotsb \tensor w_n)
	=
	w_1 \tensor \dotsb \tensor w_{i-1} \tensor (x \act w_i) \tensor w_{i+1} \tensor \dotsb \tensor w_n
\]
for all~$w_1, \dotsc, w_n \in V$.
We have for the element~$t \defined v_1 \tensor \dotsb \tensor v_n$ that
\[
	T_i( t \act \sigma )
	=
	T_{\sigma(i)}( t ) \act \sigma \,.
\]
(To see this, note that we permute the tensor factors in the same way on both sides, and also act on the same tensor factor~$v_{\sigma(i)}$ on both sides.)
It follows that
\begin{align*}
	x \act u
	&=
	x \act \bigl( t - (t \act \sigma) \bigr)
	\\
	&=
	x \act t - x \act (t \act \sigma)
	\\
	&=
	\sum_{i=1}^n T_i(t)
	- \sum_{i=1}^n T_i(t \act \sigma)
	\\
	&=
	\sum_{i=1}^n T_i(t)
	- \sum_{i=1}^n T_{\sigma(i)}(t) \act \sigma
	\\
	&=
	\sum_{i=1}^n T_i(t)
	- \sum_{i=1}^n T_i(t) \act \sigma
	\\
	&=
	\sum_{i=1}^n \bigl( T_i(t)- T_i(t) \act \sigma \bigr) \,.
\end{align*}
The terms~$T_i(t) - T_i(t) \act \sigma$ are contained in~$I$ by definition of~$I$.
This shows that the element~$x \act u$ is contained in~$I$, as required.




\subsection{}

Let~$e_1$,~$e_2$ be the standard basis of~$V$.
We have
\[
	h \act e_1 = e_1
	\quad\text{and}\quad
	h \act e_2 = -e_2 \,.
\]
The induced basis of the symmetric power~$\Symm^n(V)$ is given by the elements
\[
	v_m
	\defined
	e_1^{n-m} e_2^m
	=
	\underbrace{e_1 \dotsm e_1}_{n-m} \underbrace{e_2 \dotsm e_2}_{m} \,.
\]
with~$m = 0, \dotsc, n$.
The element~$h$ acts on these basis element by
\begin{align*}
	h \act v_m
	&=
	h \act ( \underbrace{e_1 \dotsm e_1}_{n-m} \underbrace{e_2 \dotsm e_2}_{m} )
	\\
	&=
	\sum_{i=0}^{n-m-1}
	\underbrace{e_1 \dotsm e_1}_i {} \cdot (h \act e_1) \cdot \underbrace{e_1 \dotsm e_1}_{n-m-i-1} e_2^m
	+
	\sum_{j=0}^{m-1}
	e_1^{n-m} \underbrace{e_2 \dotsm e_2}_j {} \cdot (h \act e_2) \cdot \underbrace{e_2 \dotsm e_2}_{m-j-1}
	\\
	&=
	\sum_{i=0}^{n-m-1}
	\underbrace{e_1 \dotsm e_1}_i {} \cdot e_1 \cdot \underbrace{e_1 \dotsm e_1}_{n-m-i-1} e_2^m
	-
	\sum_{j=0}^{m-1}
	e_1^{n-m} \underbrace{e_2 \dotsm e_2}_j {} \cdot e_2 \cdot \underbrace{e_2 \dotsm e_2}_{m-j-1}
	\\
	&=
	(n-m) v_m - m v_m
	\\
	&=
	(n - 2m) v_m \,.
\end{align*}





\subsection{}

We have
\begin{align*}
	e \act v_m
	&=
	e \act ( \underbrace{e_1 \dotsm e_1}_{n-m} \underbrace{e_2 \dotsm e_2}_{m} )
	\\
	&=
	\sum_{i=0}^{n-m-1}
	\underbrace{e_1 \dotsm e_1}_i {} \cdot (e \act e_1) \cdot \underbrace{e_1 \dotsm e_1}_{n-m-i-1} e_2^m
	+
	\sum_{j=0}^{m-1}
	e_1^{n-m} \underbrace{e_2 \dotsm e_2}_j {} \cdot (e \act e_2) \cdot \underbrace{e_2 \dotsm e_2}_{m-j-1}
	\\
	&=
	\sum_{i=0}^{n-m-1}
	\underbrace{e_1 \dotsm e_1}_i {} \cdot 0 \cdot \underbrace{e_1 \dotsm e_1}_{n-m-i-1} e_2^m
	+
	\sum_{j=0}^{m-1}
	e_1^{n-m} \underbrace{e_2 \dotsm e_2}_j {} \cdot e_1 \cdot \underbrace{e_2 \dotsm e_2}_{m-j-1}
	\\
	&=
	m e_1^{n-m+1} e_2^{m-1}
	\\
	&=
	m v_{m-1}
\end{align*}
for all~$m = 0, \dotsc, n$ (where in the case~$m = 0$ the expression~$0 \cdot v_{-1}$ equals zero.)
We similarly have
\begin{align*}
	f \act v_m
	&=
	f \act ( \underbrace{e_1 \dotsm e_1}_{n-m} \underbrace{e_2 \dotsm e_2}_{m} )
	\\
	&=
	\sum_{i=0}^{n-m-1}
	\underbrace{e_1 \dotsm e_1}_i {} \cdot (f \act e_1) \cdot \underbrace{e_1 \dotsm e_1}_{n-m-i-1} e_2^m
	+
	\sum_{j=0}^{m-1}
	e_1^{n-m} \underbrace{e_2 \dotsm e_2}_j {} \cdot (f \act e_2) \cdot \underbrace{e_2 \dotsm e_2}_{m-j-1}
	\\
	&=
	\sum_{i=0}^{n-m-1}
	\underbrace{e_1 \dotsm e_1}_i {} \cdot e_2 \cdot \underbrace{e_1 \dotsm e_1}_{n-m-i-1} e_2^m
	-
	\sum_{j=0}^{m-1}
	e_1^{n-m} \underbrace{e_2 \dotsm e_2}_j {} \cdot 0 \cdot \underbrace{e_2 \dotsm e_2}_{m-j-1}
	\\
	&=
	(n-m) \cdot e_1^{n-m-1} e_2^{m+1}
	\\
	&=
	(n-m) v_{m+1}
\end{align*}
for all~$m = 0, \dotsc, n$ (where in the case~$m = n$ the expression~$0 \cdot v_{n+1}$ equals zero.)

We know from the classification of irreducible, finite-dimensional representations of~$\sllie(2, \Complex)$ that there exists for every natural number~$n$ precisely one such representation~$\Irr(n)$ of dimension~$n + 1$ (and highest weight~$n$), and that~$\Irr(n)$ admits a basis~$w_0, \dotsc, w_n$ on which the actions of~$e$,~$h$,~$f$ are given by
\[
	e \act w_m
	=
	m w_{m-1} \,,
	\quad
	h \act w_m
	=
	(n - 2m) w_m \,,
	\quad
	f \act w_m
	=
	(n-m) w_{m+1}
\]
for all~$m = 0, \dotsc, n$.
We find from the above calculations that the representations~$\Irr(n)$ and~$\Symm^n(V)$ are isomorphic in such a way that~$v_m$ corresponds to~$w_m$ for all~$m = 0, \dotsc, n$.
The exercise now follows from the classification of finite-dimensional, irreducible representations of~$\sllie(2, \Complex)$.





