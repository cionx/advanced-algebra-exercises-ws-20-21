\section{}



\subsection{}

\subsubsection*{The commutator ideal of~$\gllie(n, \kf)$}

\begin{recall}
	\label{matrices commute under trace}
	For any two natural numbers~$n$ and~$m$, every matrix~$A$ in~$\Mat(m \times n, \kf)$, and every matrix~$B$ in~$\Mat(n \times m, \kf)$ we have
	\[
		\tr(AB)
		=
		\sum_{i=1}^m (AB)_{ii}
		=
		\sum_{i=1}^m \sum_{j=1}^n A_{ij} B_{ji}
		=
		\sum_{j=1}^n \sum_{i=1}^n B_{ji} A_{ij}
		=
		\sum_{j=1}^n (BA)_{jj}
		=
		\tr(BA) \,.
	\]
\end{recall}

It follows from \cref{matrices commute under trace} for any two matrices~$A$ and~$B$ in~$\gllie(n, \kf)$ that
\[
	\tr( [A, B] )
	=
	\tr( AB - BA )
	=
	\tr(AB) - \tr(BA)
	=
	0 \,.
\]
This shows that the commutator ideal~$[ \gllie(n, \kf), \gllie(n, \kf) ]$ is contained in~$\sllie(n, \kf)$.
We know on the other hand that~$\sllie(n, \kf)$ has a basis given by the matrices~$E_{ij}$ with~$i, j = 1, \dotsc, n$ and~$i \neq j$, together with the matrices~$E_{ii} - E_{i+1, i+1}$ for~$i = 1, \dotsc, n-1$.
The first kind of basis matrix can be written as
\[
	E_{ij}
	=
	E_{ii} E_{ij} - \underbrace{ E_{ij} E_{ii} }_{=0}
	=
	[ E_{ii}, E_{ij} ] \,,
\]
and the second kind of basis matrix as
\[
	E_{ii} - E_{i+1, i+1}
	=
	E_{i, i+1} E_{i+1, i} - E_{i+1, i} E_{i, i+1}
	=
	[ E_{i, i+1}, E_{i+1, i} ] \,.
\]
This shows that the basis matrices of~$\sllie(n, \kf)$ can be written as commutators in~$\gllie(n, \kf)$.

This shows altogether that the commutator ideal of~$\gllie(n, \kf)$ is given by~$\sllie(n, \kf)$.

\subsubsection*{The commutator ideal of~$\sllie(n, \kf)$}

\begin{proposition}
	\label{simple lie algebras are perfect}
	Every simple Lie~algebra~$\glie$ is perfect, i.e. satisfies~$\glie = [\glie, \glie]$.
\end{proposition}

\begin{proof}
	The Lie~algebra~$\glie$ is nonabelian because it is simple.
	The commutator ideal~$[ \glie, \glie ]$ is therefore nonzero.
	But this is an ideal in~$[ \glie, \glie ]$, so it must already equal~$\glie$ itself because~$\glie$ is simple.
\end{proof}

We have seen on Exercise~Sheet~2 that~$\sllie(n, \kf)$ is simple whenever the characteristic of~$\kf$ does not divide~$n$.
It then follows from \cref{simple lie algebras are perfect} that~$[ \sllie(n, \kf), \sllie(n, \kf) ] = \sllie(n, \kf)$.
This is particular the case if the characteristic of~$\kf$ is zero, as given in this exercise.





\subsection{}

\subsubsection*{The Lie~algebra~$\upperlie(n, \kf)$ is nilpotent}

Let~$\upperlie \defined \upperlie(n, \kf)$, and for every natural number~$k$ let
\[
	\upperlie_k
	\defined
	\gen{
		E_{ij}
	\suchthat
		\text{$i, j = 1, \dotsc, n$ with~$i \leq j-k$}
	}_{\kf} \,.
\]
This is the space of all those strictly upper triangular matrices for which the first~$k$ upper diagonals vanish.
(We count the main diagonal as an upper diagonal.)
A matrix~$A$ of size~$n \times n$ is contained in~$\upperlie_k$ if and only if
\[
	A e_i
	\subseteq
	\gen{ e_1, \dotsc, e_{i-k} }_{\kf}
\]
for all~$i = 1, \dotsc, n$, where~$e_1, \dotsc, e_n$ denotes the standard basis of~$\kf^n$.
It follows from the first description that~$\upperlie_1 = \upperlie$, and it follows from the second description that
\[
	\upperlie_k \cdot \upperlie_l
	\subseteq
	\upperlie_{k+l}
\]
for all~$k, l \geq 0$, and therefore also
\[
	[ \upperlie_k, \upperlie_l ]
	\subseteq
	\upperlie_{k+l}
\]
for all~$k, l \geq 0$.
It now follows from~$\upperlie = \upperlie_1$ by induction that
\[
	\upperlie^i
	\subseteq
	\upperlie_{i+1}
\]
for all~$i \geq 0$.
(The shift comes from our convention~$\upperlie^0 = \upperlie$.)
We have~$\upperlie_k = 0$ for every~$k \geq n$, whence we have shown that~$\upperlie$ is nilpotent.

\subsubsection*{The Lie~algebra~$\trianglie(n, \kf)$ is solvable}

For any two matrices~$A$ and~$B$ in~$\trianglie(n, \kf)$ both their products~$AB$ and~$BA$ are again upper triangular, and both products have the same diagonal.
The commutator~$[A, B] = AB - BA$ is therefore a strictly upper triangular matrix.
The derived algebra~$\trianglie(n, \kf)^{(1)} = [ \trianglie(n, \kf), \trianglie(n, \kf)]$ is thus contained in~$\upperlie(n, \kf)$.
We have previously shown that this Lie~algebra is nilpotent, and thus solvable.
It follows that~$\trianglie(n, \kf)$ is solvable.%
\footnote{
	We use here that a Lie~algebra~$\glie$ is solvable if and only if for some~$i \geq 0$ the term~$\glie^{(i)}$ is solvable.
}



\subsection{}

Let~$\glie$ be the Lie~algebra~$\sllie(2, \kf)$ where~$\kf$ is some field of characteristic~$2$.
The Lie~bracket of~$\sllie(2, \kf)$ is on the standard basis~$e$,~$h$,~$f$ of~$\sllie(2, \kf)$ given by
\[
	[h, e] = 2 e = 0 \,,
	\quad
	[h, f] = -2 f = 0 \,,
	\quad
	[e, f] = h \,.
\]
It follows that the Lie~algebra~$\glie^1 = [ \glie, \glie ]$ is one-dimensional and spanned by~$h$.
It further follows that~$\glie^2 = [\glie, \glie^1]$ is spanned by the three elements
\[
	[h, e] = 0 \,,
	\quad
	[h, h] = 0 \,,
	\quad
	[h, f] = 0 \,.
\]
This shows that~$\glie^2$ vanishes, which shows that~$\glie$ is nilpotent.





