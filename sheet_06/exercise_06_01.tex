\section{}

For every representation~$V$ of~$\sllie(2, \Complex)$ and every complex scalar~$\lambda$, we denote the~\weightspace{$\lambda$} of~$V$ by
\[
	V_\lambda
	\defined
	\{
		v \in V
	\suchthat
		h \act v = \lambda v
	\} \,.
\]

Let~$V$ be a finite-dimensional~\representation{$\sllie(2, \Complex)$}.
We know from Weyl’s theorem that~$V$ is semisimple, and thus decomposes into a direct sum of irreducible, finite-dimensional representations.
We know from the classification of these irreducible representations that each irreducible direct summand of~$V$ decomposes into weight spaces, and that all occuring weights are integers.
We hence find that every finite-dimensional representation of~$\sllie(2, \Complex)$ decomposes into weight spaces, with all occurings weights being integral.

We call the Laurent polynomial~$\ch(V)$ in~$\Integer[t, t^{-1}]$ given by
\[
	\ch(V)
	\defined
	\sum_{\lambda \in \Integer}
	\dim(V_\lambda) t^\lambda
\]
the \defemph{character} if~$V$.
We observe the following four properties of characters.

\begin{proposition}
	Let~$V$ and~$W$ be two finite-dimensional representations of~$\sllie(2, \Complex)$.
	\begin{enumerate}
		\item
			Suppose that~$V$ and~$W$ are isomorphic.
			Then~$\ch(V) = \ch(W)$.
		\item
			We have
			\[
				\ch(\Irr(n))
				=
				t^n + t^{n-2} + \dotsb + t^{-n+2} + t^{-n}
				=
				\frac{t^{n+1} - t^{-n-1}}{t - t^{-1}}
			\]
			in~$\Rational(t)$ for every natural number~$n$.
		\item
			We have~$\ch(V \oplus W) = \ch(V) + \ch(W)$.
		\item
			We have~$\ch(V \tensor W) = \ch(V) \cdot \ch(W)$.
	\end{enumerate}
\end{proposition}


\begin{proof}
			Let~$V = \bigoplus_{\lambda \in \Integer} V_\lambda$ and~$W = \bigoplus_{\lambda \in \Integer} W_\lambda$ be the weight space decompositions of~$V$ and~$W$.
	\begin{enumerate}[start=2]
		\item
			The identity
			\[
				\ch(\Irr(n))
				=
				t^n
				+
				t^{n-2}
				+
				\dotsb
				+
				t^{-n+2}
				+
				t^{-n}
			\]
			follows from the explicit description of~$\Irr(n)$ that was given in the lectures.
			It follows from the telescoping sum
			\[
				( t^n + t^{n-2} + \dotsb + t^{-n-2} + t^{-n} ) \cdot (t - t^{-1})
				=
				t^{n+1} - t^{-n-1}
			\]
			that
			\[
				\ch(\Irr(n))
				=
				\frac{t^{n+1} - t^{-n-1}}{t - t^{-1}} \,.
			\]
		\item
			We have for every weight~$\lambda$ the inclusion
			\[
				V_\lambda \oplus W_\lambda
				\subseteq
				(V \oplus W)_\lambda \,.
			\]
			But we have
			\[
				V \oplus W
				=
				\Biggl(
					\bigoplus_{\lambda \in \Integer}
					V_\lambda
				\Biggr)
				\oplus
				\Biggl(
					\bigoplus_{\lambda \in \Integer}
					W_\lambda
				\Biggr)
				=
				\bigoplus_{\lambda \in \Integer}
				{}
				( V_\lambda \oplus W_\lambda )
				\subseteq
				\bigoplus_{\lambda \in \Integer}
				{}
				(V \oplus W)_\lambda
				=
				V \oplus W \,,
			\]
			whence each inclusion~$V_\lambda \oplus W_\lambda \subseteq (V \oplus W)_\lambda$ must already be an equality.
			We hence find that
			\[
				(V \oplus W)_\lambda
				=
				V_\lambda \oplus W_\lambda
				\qquad
				\text{for every~$\lambda \in \Integer$,}
			\]
			and therefore
			\begin{align*}
				\SwapAboveDisplaySkip
				\ch(V \oplus W)
				&=
				\sum_{\lambda \in \Integer}
				\dim( (V \oplus W)_\lambda ) t^\lambda
				\\
				&=
				\sum_{\lambda \in \Integer}
				\dim( V_\lambda \oplus W_\lambda ) t^\lambda
				\\
				&=
				\sum_{\lambda \in \Integer}
				\bigl( \dim(V_\lambda) + \dim(W_\lambda) \bigr) t^\lambda
				\\
				&=
				\sum_{\lambda \in \Integer}
				 \dim(V_\lambda) t^\lambda
				+
				\sum_{\lambda \in \Integer}
				\dim(W_\lambda) t^\lambda
				\\
				&=
				\ch(V) + \ch(W) \,.
			\end{align*}
		\item
			For any two weights~$\lambda$ and~$\mu$ and every two vectors~$v \in V_\lambda$ and~$w \in W_\mu$ we have
			\[
				h \act (v \tensor w)
				=
				(h \act v) \tensor w + v \tensor (h \act w)
				=
				\lambda v \tensor w + \mu v \tensor w
				=
				(\lambda + \mu) v \tensor w \,.
			\]
			This shows that
			\[
				V_\lambda \tensor W_\mu
				\subseteq
				(V \tensor W)_{\lambda + \mu}
			\]
			for all~$\lambda, \mu \in \Integer$.
			But we have
			\begin{align*}
				V \tensor W
				&=
				\Biggl(
					\bigoplus_{\lambda \in \Integer}
					V_\lambda
				\Biggr)
				\tensor
				\Biggl(
					\bigoplus_{\mu \in \Integer}
					W_\mu
				\Biggr)
				\\
				&=
				\bigoplus_{\lambda, \mu \in \Integer}
				{}
				( V_\lambda \tensor W_\mu )
				\\
				&=
				\bigoplus_{\kappa \in \Integer}
				\bigoplus_{\lambda + \mu = \kappa}
				{}
				( V_\lambda \tensor W_\mu )
				\\
				&\subseteq
				\bigoplus_{\kappa \in \Integer}
				{}
				(V \tensor W)_\kappa
				\\
				&=
				V \tensor W \,,
			\end{align*}
			which shows that~$(V \tensor W)_\kappa = \bigoplus_{\lambda + \mu = \kappa} V_\lambda \tensor W_\mu$ for every~$\kappa \in \Integer$.
			It follows that
			\begin{align*}
				\ch(V \tensor W)
				&=
				\sum_{\kappa \in \Integer}
				\dim( (V \tensor W)_\kappa ) t^{\kappa}
				\\
				&=
				\sum_{\kappa \in \Integer}
				\dim
				\Biggl(
					\bigoplus_{\lambda + \mu = \kappa}
					V_\lambda \tensor W_\mu
				\Biggr)
				t^{\kappa}
				\\
				&=
				\sum_{\kappa \in \Integer}
				{}
				\Biggl(
					\sum_{\lambda + \mu = \kappa}
					\dim( V_\lambda \tensor W_\mu )
				\Biggr)
				t^{\kappa}
				\\
				&=
				\sum_{\kappa \in \Integer}
				{}
				\Biggl(
					\sum_{\lambda + \mu = \kappa}
					\dim( V_\lambda ) \dim( W_\mu )
				\Biggr)
				t^{\kappa}
				\\
				&=
				\sum_{\lambda, \mu \in \Integer}
				\dim(V_\lambda) \dim(W_\mu) t^{\lambda + \mu}
				\\
				&=
				\Biggl(
					\sum_{\lambda \in \Integer}
					\dim( V_\lambda ) t^\lambda
				\Biggr)
				\cdot
				\Biggl(
					\sum_{\mu \in \Integer}
					\dim( W_\mu ) t^\mu
				\Biggr)
				\\
				&=
				\ch(V) \cdot \ch(W) \,,
			\end{align*}
			as claimed.
		\qedhere
	\end{enumerate}
\end{proof}

\begin{corollary}
	Let~$V_1, \dotsc, V_r$ be finite-dimensional representations of~$\sllie(2, \Complex)$.
	\begin{enumerate}
		\item
			We have~$\ch(V_1 \oplus \dotsb \oplus V_r) = \ch(V_1) + \dotsb + \ch(V_r)$.
		\item
			We have~$\ch(V_1 \tensor \dotsb \tensor V_r) = \ch(V_1) \dotsm \ch(V_r)$.
		\qed
	\end{enumerate}
\end{corollary}

We can now solve the problem by pure calculations.

\begin{corollary}
	\label{character determines representation}
	Let~$V$ be a finite-dimensional~\representation{$\sllie(2, \Complex)$}.
	The character of~$V$ determines the isomorphism class of~$V$.
\end{corollary}

\begin{proof}
	We consider the decomposition of~$V$ into irreducible representations, given by
	\[
		V
		\cong
		\bigoplus_{n \in \Natural}
		\Irr(n)^{\oplus r_n} \,.
	\]
	It sufficies to show that the numbers~$r_n$ for~$n \in \Natural$ are uniquely determined by the character of~$V$.
	For this, we note that
	\[
		\ch(V)
		=
		\sum_{n \in \Natural} r_n \ch(\Irr(n)) \,.
	\]
	The characters~$\ch(\Irr(n))$ with~$n \in \Natural$ are monic Laurent polynomials of pairwise distinct degrees.
	These characters are therefore linearly independent.
	The coefficients~$r_n$ are therefore uniquely determined by~$\ch(V)$.
\end{proof}

\begin{proposition}
	Let~$m$ and~$n$ be two natural numbers with~$m \geq n$.
	Then
	\[
		\Irr(m) \tensor \Irr(n)
		\cong
		\Irr(m+n) \oplus \Irr(m+n-2) \oplus \dotsb \oplus \Irr(m-n+2) \oplus \Irr(m-n) \,.
	\]
\end{proposition}

\begin{proof}
	We have
	\begingroup
	\addtolength{\jot}{0.5em}
	\begin{align*}
		{}&
		\ch( \Irr(m) \tensor \Irr(n) )
		\\
		={}&
		\ch( \Irr(m) ) \cdot \ch( \Irr(n) )
		\\
		={}&
		\frac{ t^{m+1} - t^{-m-1} }{t - t^{-1}}
		\cdot
		\frac{ t^{n+1} - t^{-n-1} }{t - t^{-1}}
		\\
		={}&
		\frac{ t^{m+n+2} - t^{m-n} - t^{n-m} + t^{-n-m-2} }{ ( t - t^{-1} )^2 }
		\\
		={}&
		\frac{ ( t^{m+n+2} - t^{m-n} ) - ( t^{n-m} - t^{-n-m-2} ) }{ ( t - t^{-1} )^2 }
		\\
		={}&
		\frac
		{
			\splitfrac
			{
				(t^{m+n+1} + t^{m+n-1} + \dotsb + t^{m-n+3} + t^{m-n+1}) (t - t^{-1})
			}
			{
				- (t^{n-m-1} + t^{n-m-3} + \dotsb + t^{-n-m+1} + t^{-n-m-1})(t - t^{-1})
			}
		}
		{
			( t - t^{-1} )^2
		}
		\\
		={}&
		\frac
		{
			t^{m+n+1} + t^{m+n-1} + \dotsb + t^{m-n+3} + t^{m-n+1}
			- t^{n-m-1} - t^{n-m-3} - \dotsb - t^{-n-m+1} - t^{-n-m-1}
		}
		{
			t - t^{-1}
		}
		\\
		={}&
		\frac
		{
			(t^{m+n+1} - t^{-m-n-1})
			+ (t^{m+n-1} - t^{-n-m+1})
			+ \dotsb
			+ (t^{m-n+3} - t^{n-m-3})
			+ (t^{m-n+1} - t^{n-m-1})
		}
		{
			t - t^{-1}
		}
		\\
		={}&
		\frac{t^{m+n+1} - t^{-m-n-1}}{t - t^{-1}}
		+ \frac{t^{m+n-1} - t^{-m-n+1}}{t - t^{-1}}
		+ \dotsb
		+ \frac{t^{m-n+3} - t^{n-m-3}}{t - t^{-1}}
		+ \frac{t^{m-n+1} - t^{n-m-1}}{t - t^{-1}}
		\\
		={}&
		\ch( \Irr(m+n) )
		+ \ch( \Irr(m+n-2) )
		+ \dotsb
		+ \ch( \Irr(m-n+2) )
		+ \ch( \Irr(m-n) )
		\\
		={}&
		\ch\Bigl( \Irr(m+n) \oplus \Irr(m+n-2) \oplus \dotsb \oplus \Irr(m-n+2) \oplus \Irr(m-n) \Bigr)
	\end{align*}
	\endgroup
	The proposed isomorphism follows therefore from \cref{character determines representation}.
\end{proof}





